package com.app.talvido.business.data.network

import android.util.Log
import com.app.talvido.business.localData.PrefsManager
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response


class AuthenticationInterceptor :
    Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response = chain.request().let {

        val authKey = PrefsManager.get().getString(PrefsManager.PREF_AUTH_TOKEN, null)
        Log.i("myAuthhKey", "intercept: $authKey")
        val newRequest: Request?

        newRequest = if (authKey != null) {
            it.newBuilder()
                .addHeader("Authorization", authKey)
                .build()
        } else {
            it.newBuilder()
                .build()
        }
        chain.proceed(newRequest)
    }
}