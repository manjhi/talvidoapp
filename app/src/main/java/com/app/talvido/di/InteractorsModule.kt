package com.app.talvido.di

import com.app.talvido.framwork.datasource.interactors.UserUseCase
import com.app.talvido.framwork.datasource.retrofit.AppApiServices
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton



@Module
@InstallIn(ApplicationComponent::class)
object InteractorsModule {

    @Singleton
    @Provides
    fun provideGetUserCase(
        networkDataSource: AppApiServices
    ): UserUseCase {
        return UserUseCase( networkDataSource)
    }
}