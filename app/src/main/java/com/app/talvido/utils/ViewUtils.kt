package com.app.talvido.utils

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.databinding.BindingAdapter
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.talvido.R
import com.app.talvido.base.delegate.MyApplication
import com.bumptech.glide.Glide
import com.google.firebase.messaging.FirebaseMessaging


const val INTRO_STRING_OBJECT = "INTRO_STRING_OBJECT"


fun View.Gone() {
    this.visibility = View.GONE
}

fun View.Visible() {
    this.visibility = View.VISIBLE
}


fun View.InVisible() {
    this.visibility = View.INVISIBLE
}


fun View.onClick(action: (v: View) -> Unit) {
    this.setOnClickListener {
        action.invoke(it)
    }
}

@SuppressLint("HardwareIds")
fun deviceId(): String {
    return Settings.Secure.getString(
       MyApplication.application.contentResolver,
        Settings.Secure.ANDROID_ID
    )
}
fun FragmentActivity.explain() {
    val dialog = AlertDialog.Builder(this)
    dialog.setMessage("You need to give some mandatory permissions to continue. Do you want to go to app settings?")
        .setPositiveButton("Yes") { paramDialogInterface, _ ->
            paramDialogInterface.dismiss()
            Intent(
                Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.parse("package:" + this.packageName)
            ).also {
                this.startActivity(it)
            }
        }
        .setNegativeButton("Cancel"
        ) { paramDialogInterface, _ -> paramDialogInterface.dismiss() }
    dialog.show()
}


fun String.toast() {
    Toast.makeText(MyApplication.application.applicationContext, this, Toast.LENGTH_LONG).show()
}

fun RecyclerView.setManager(
    isItHorizontal: Boolean = false,
    isItGrid: Boolean = false,
    spanCount: Int = 2
) {
    if (isItGrid)
        this.layoutManager = GridLayoutManager(this.context, spanCount)
    else {
        if (isItHorizontal)
            this.layoutManager = LinearLayoutManager(this.context, RecyclerView.HORIZONTAL, false)
        else
            this.layoutManager = LinearLayoutManager(this.context, RecyclerView.VERTICAL, false)
    }
}

@BindingAdapter("image")
fun ImageView.loadImage(path: String?) {
    path?.let {
        Glide.with(this).load(it).placeholder(R.drawable.user_icon)
            .error(R.drawable.user_icon).into(this)
    }
    if (path.isNullOrEmpty()) {
        this.setImageResource(R.drawable.user_icon)
    }
}