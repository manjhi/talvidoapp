package com.app.talvido.utils

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Gravity
import android.view.View
import android.view.Window
import android.widget.FrameLayout
import android.widget.LinearLayout
import com.app.talvido.R
import kotlinx.android.synthetic.main.custom_alert_dialog.*
import kotlinx.android.synthetic.main.custom_alert_dialog.view.*

class CustomAlertDialog(

    var title: String?,
    var positiveButton: String?,
    var negativeButton: String?,
    var mContext: Context?,
    internal var listenr: CallbackCustomDialog
) :
    View.OnClickListener {

    private var mDialog: Dialog? = null

    override fun onClick(p0: View?) {
    }

    lateinit var view: View
    fun showDialog() {
        if (mDialog == null) {
            mDialog = Dialog(mContext!!)
            mDialog!!.window!!.requestFeature(Window.FEATURE_NO_TITLE)
            view = View.inflate(mContext, R.layout.custom_alert_dialog, null)

            mDialog!!.setContentView(view)
            mDialog!!.setCancelable(false)
            mDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            mDialog!!.window!!.setLayout(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            mDialog!!.window!!.setGravity(Gravity.CENTER)
            mDialog!!.window!!.attributes.windowAnimations =
                R.style.DialogUpTheme
            val params =
                mDialog!!.window!!.attributes
            params.y = 3 // Here is the param to set your dialog position. Same with params.x

            params.x = 3 // Here is the param to set your dialog position. Same with params.Y

            mDialog!!.window!!.attributes = params
            val paramsRoot =
                FrameLayout.LayoutParams(
                    FrameLayout.LayoutParams.MATCH_PARENT,
                    FrameLayout.LayoutParams.WRAP_CONTENT
                )
            paramsRoot.setMargins(0, 0, 0, 0);

            view.rlRoot.setLayoutParams(paramsRoot);

            mDialog!!.tvTitle.setText(title)
            mDialog!!.tvPostiveButton.setText(positiveButton)
            mDialog!!.tvNegativeButton.setText(negativeButton)

            mDialog!!.show();

            mDialog!!.PostiveButton.setOnClickListener {
                listenr.onPostiveButton()
                dismiss()
            }
            mDialog!!.NegativeButton.setOnClickListener {
                listenr.onNegativeButton()
                dismiss()
            }


        } else {

        }
    }


    public fun dismiss() {
        mDialog?.dismiss()
        mDialog = null
    }


    interface CallbackCustomDialog {
        fun onNegativeButton()
        fun onPostiveButton()
    }
}