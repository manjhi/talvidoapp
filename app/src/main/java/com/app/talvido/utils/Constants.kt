package com.app.talvido.utils

class Constants {

    companion object {
        val GPS_REQ_CODE: Int=3100
        val LIVE_VIEWERS: String = "Viewers"
        val LIVE_GIFTS: String = "Gifts"
        val GIFTS_LIST: String = "GiftsList"
        val LIVE_USERS: String = "LiveUsers"
        val USERS: String = "Users"
        const val appID = 3263683827L
        const val appSign = "3037a62076bdba82b9af59e85c9b9929709cd874018cc864df4f6a266d2951e2"
        const val LIVE_USER_DATA = "live_user_data"
        var latitude: Double? = 0.0
        var longitude: Double? = 0.0
        val auth_token: String = ""
    }
}