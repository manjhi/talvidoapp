package com.app.talvido.liveStream

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.app.talvido.business.localData.PrefsManager
import com.app.talvido.framwork.datasource.retrofit.ApiService
import com.app.talvido.framwork.presentation.navHost.MainActivity
import com.app.talvido.liveStream.network.ApiHitAndHandle
import com.app.talvido.liveStream.network.RetrofitClient
import com.app.talvido.utils.LoadingDialog
import com.google.android.gms.location.R

abstract class LiveBaseActivity : AppCompatActivity() {
    var service: ApiService? = null
    var apiParse: ApiHitAndHandle? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        service = RetrofitClient(this).getClient().create(ApiService::class.java)
        apiParse = ApiHitAndHandle()

    }

    private val loadingDialog: LoadingDialog by lazy {
        LoadingDialog(this@LiveBaseActivity)
    }

    fun showToast(msg: String) {
        Toast.makeText(this@LiveBaseActivity, msg, Toast.LENGTH_SHORT).show()
    }

    fun showLoading() {
        loadingDialog.setLoading(true)
    }

    fun hideLoading() {
        loadingDialog.setLoading(false)
    }

    fun isInternetConnected(): Boolean {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        if (networkInfo != null && networkInfo.isConnected) {
        } else {
            noInternetConnected()
        }
        return networkInfo != null && networkInfo.isConnected
    }

    private fun noInternetConnected() {
        showToast("No Internet")
    }

    fun logoutFromApp() {
        PrefsManager.get().removeAll()
        startActivity(Intent(this@LiveBaseActivity, MainActivity::class.java))
        finishAffinity()
    }
}