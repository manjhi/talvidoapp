package com.app.talvido.liveStream.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ModelUser(
    var roomName: String,
    var liveUserName: String,
    var streamId: String
) : Parcelable
