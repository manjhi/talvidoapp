package com.app.talvido.liveStream.network

import com.google.gson.JsonObject
import retrofit2.Call

interface ApiResponseLive {

    fun onSuccess(call: Call<JsonObject>, responseCode: Int, response: String)

    fun onError(call: Call<JsonObject>, errorCode: Int, errorMsg: String)
}