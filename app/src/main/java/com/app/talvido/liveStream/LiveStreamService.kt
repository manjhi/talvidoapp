package com.app.talvido.liveStream

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.IBinder
import android.util.Log
import com.app.talvido.business.localData.PrefsManager
import com.app.talvido.framwork.datasource.retrofit.ApiService
import com.app.talvido.liveStream.network.ApiHitAndHandle
import com.app.talvido.liveStream.network.ApiResponseLive
import com.app.talvido.liveStream.network.RetrofitClient
import com.google.gson.JsonObject
import retrofit2.Call

class LiveStreamService : Service(), ApiResponseLive {
    var service: ApiService? = null
    var apiParse: ApiHitAndHandle? = null
    lateinit var callStopLive: Call<JsonObject>
    lateinit var context: Context
    lateinit var liveId: String
    var TAG: String = "liveSTreamStream"
    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        context = applicationContext
        service = RetrofitClient(this).getClient().create(ApiService::class.java)
        apiParse = ApiHitAndHandle()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        liveId = intent!!.extras!!.getString("liveId", "")
        Log.i(TAG, "onStartCommand: in service $liveId")
        PrefsManager.get().save("liveId", liveId)
        return START_STICKY
    }


    override fun onDestroy() {
        stopLiveStream()
        super.onDestroy()
    }

    private fun stopLiveStream() {
        Log.i(TAG, "stopLiveStream: entered function")
        var myliveId = PrefsManager.get().getString("liveId", "")!!
        callStopLive = service!!.stopLiveStreaming(myliveId)
        apiParse!!.hitApi(callStopLive, false, context, this, true)
    }

    override fun onSuccess(call: Call<JsonObject>, responseCode: Int, response: String) {
        stopSelf()   // stop service
        Log.i(TAG, "onSuccess: notified backend service stopped")
    }

    override fun onError(call: Call<JsonObject>, errorCode: Int, errorMsg: String) {
        Log.i(TAG, "onSuccess: error service stoppinh $errorMsg")
    }

}