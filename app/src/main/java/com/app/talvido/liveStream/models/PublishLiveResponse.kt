package com.app.talvido.liveStream.models

data class PublishLiveResponse(
    val archivedDate: String,
    val created: String,
    val id: String,
    val liveUserCoin: String,
    val liveUserCrown: String,
    val roomName: String,
    val status: String,
    val streamId: String,
    val userId: String
)