package com.app.talvido.liveStream.player

import android.os.Bundle
import android.util.Log
import android.view.TextureView
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.talvido.liveStream.LiveBaseActivity
import com.app.talvido.liveStream.adapter.AdapterMessages
import com.app.talvido.liveStream.models.ModelBroadcastMessage
import com.app.talvido.utils.Constants
import com.app.talvido.R
import im.zego.zegoexpress.ZegoExpressEngine
import im.zego.zegoexpress.callback.IZegoEventHandler
import im.zego.zegoexpress.constants.*
import im.zego.zegoexpress.entity.*
import org.json.JSONObject
import java.util.*

class PlayStreamActivity : LiveBaseActivity(), AdapterMessages.MessageCallback {
    lateinit var engine: ZegoExpressEngine
    private var zegoCanvas: ZegoCanvas? = null
    var play_view: TextureView? = null
    var viewMode = ZegoViewMode.ASPECT_FILL
    private val ROOM_NAME = "LiveRoom"
    val TAG = "PlayliveActivity"
    private val streamID = "sbdbfkjsdbf"
    private var resourceMode = ZegoStreamResourceMode.DEFAULT
    lateinit var etPlayerSendMessage: EditText
    lateinit var rvPlayerMessages: RecyclerView
    var listMessages: ArrayList<ModelBroadcastMessage> = ArrayList()
    val user = ZegoUser("MishuMittal", "Bagicha Singh")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_play_stream)

        initIds()
        initEngine()
        setUpMessagesRecycler()

        if (engine != null) {
            Toast.makeText(this, "Engine Initailized", Toast.LENGTH_SHORT).show()
        }
        engine.loginRoom(ROOM_NAME, user, null)
        zegoCanvas = ZegoCanvas(play_view)
        zegoCanvas!!.viewMode = viewMode
        val playerConfig = ZegoPlayerConfig()
        playerConfig.resourceMode = resourceMode
        engine.startPlayingStream(streamID, zegoCanvas, playerConfig)
    }


    private fun initIds() {
        play_view = findViewById<TextureView>(R.id.play_view)
        rvPlayerMessages = findViewById(R.id.rvPlayerMessages)
        etPlayerSendMessage = findViewById(R.id.etPlayerSendMessage)
    }

    fun initEngine() {
        engine = ZegoExpressEngine.createEngine(
            Constants.appID, Constants.appSign, true, ZegoScenario.GENERAL,
            application, object : IZegoEventHandler() {
                override fun onEngineStateUpdate(state: ZegoEngineState) {
                    super.onEngineStateUpdate(state)
                }

                override fun onRoomStateUpdate(
                    roomID: String,
                    state: ZegoRoomState,
                    errorCode: Int,
                    extendedData: JSONObject
                ) {
                    super.onRoomStateUpdate(roomID, state, errorCode, extendedData)
                    Log.i(TAG, "onRoomStateUpdate: $roomID $state")
                    if (state == ZegoRoomState.DISCONNECTED) {
                        onBackPressed()
                    }
                }

                override fun onRoomUserUpdate(
                    roomID: String,
                    updateType: ZegoUpdateType,
                    userList: ArrayList<ZegoUser>
                ) {
                    super.onRoomUserUpdate(roomID, updateType, userList)
                    Log.i(TAG, "onRoomUserUpdate: ")

                }

                override fun onRoomOnlineUserCountUpdate(roomID: String, count: Int) {
                    super.onRoomOnlineUserCountUpdate(roomID, count)
                    Log.i(TAG, "onRoomOnlineUserCountUpdate: ")
                }

                override fun onRoomStreamUpdate(
                    roomID: String,
                    updateType: ZegoUpdateType,
                    streamList: ArrayList<ZegoStream>
                ) {
                    super.onRoomStreamUpdate(roomID, updateType, streamList)
                    Log.i(TAG, "onRoomStreamUpdate: ")

                }

                override fun onRoomStreamUpdate(
                    roomID: String,
                    updateType: ZegoUpdateType,
                    streamList: ArrayList<ZegoStream>,
                    extendedData: JSONObject
                ) {
                    super.onRoomStreamUpdate(roomID, updateType, streamList, extendedData)
                    Log.i(
                        TAG,
                        "onRoomStreamUpdate: ${extendedData.toString()}   ${streamList.size}  ${updateType}"
                    )

                    if (updateType == ZegoUpdateType.DELETE) {
                        onBackPressed()
                    }

                }

                override fun onRoomStreamExtraInfoUpdate(
                    roomID: String,
                    streamList: ArrayList<ZegoStream>
                ) {
                    super.onRoomStreamExtraInfoUpdate(roomID, streamList)
                    Log.i(TAG, "onRoomStreamExtraInfoUpdate: ")

                }

                override fun onRoomExtraInfoUpdate(
                    roomID: String,
                    roomExtraInfoList: ArrayList<ZegoRoomExtraInfo>
                ) {
                    super.onRoomExtraInfoUpdate(roomID, roomExtraInfoList)
                    Log.i(TAG, "onRoomExtraInfoUpdate: ")
                }

                override fun onPublisherStateUpdate(
                    streamID: String,
                    state: ZegoPublisherState,
                    errorCode: Int,
                    extendedData: JSONObject
                ) {
                    super.onPublisherStateUpdate(streamID, state, errorCode, extendedData)
                    Log.i(TAG, "onPublisherStateUpdate: ")
                }

                override fun onPublisherQualityUpdate(
                    streamID: String,
                    quality: ZegoPublishStreamQuality
                ) {
                    super.onPublisherQualityUpdate(streamID, quality)
                    Log.i(TAG, "onPublisherQualityUpdate: ")
                }

                override fun onPublisherCapturedAudioFirstFrame() {
                    super.onPublisherCapturedAudioFirstFrame()
                    Log.i(TAG, "onPublisherCapturedAudioFirstFrame: ")
                }

                override fun onPublisherCapturedVideoFirstFrame(channel: ZegoPublishChannel) {
                    super.onPublisherCapturedVideoFirstFrame(channel)
                    Log.i(TAG, "onPublisherCapturedVideoFirstFrame: ")
                }

                override fun onPublisherVideoSizeChanged(
                    width: Int,
                    height: Int,
                    channel: ZegoPublishChannel
                ) {
                    super.onPublisherVideoSizeChanged(width, height, channel)
                    Log.i(TAG, "onPublisherVideoSizeChanged: ")
                }

                override fun onPublisherRelayCDNStateUpdate(
                    streamID: String,
                    infoList: ArrayList<ZegoStreamRelayCDNInfo>
                ) {
                    super.onPublisherRelayCDNStateUpdate(streamID, infoList)
                    Log.i(TAG, "onPublisherRelayCDNStateUpdate: ")
                }

                override fun onPlayerStateUpdate(
                    streamID: String,
                    state: ZegoPlayerState,
                    errorCode: Int,
                    extendedData: JSONObject
                ) {
                    super.onPlayerStateUpdate(streamID, state, errorCode, extendedData)
                    Log.i(TAG, "onPlayerStateUpdate: " + state)

                }

                override fun onPlayerQualityUpdate(
                    streamID: String,
                    quality: ZegoPlayStreamQuality
                ) {
                    super.onPlayerQualityUpdate(streamID, quality)
                    Log.i(TAG, "onPlayerQualityUpdate: ")
                }

                override fun onPlayerMediaEvent(streamID: String, event: ZegoPlayerMediaEvent) {
                    super.onPlayerMediaEvent(streamID, event)
                    Log.i(TAG, "onPlayerMediaEvent: ")
                }

                override fun onPlayerRecvAudioFirstFrame(streamID: String) {
                    super.onPlayerRecvAudioFirstFrame(streamID)
                    Log.i(TAG, "onPlayerRecvAudioFirstFrame: ")
                }

                override fun onPlayerRecvVideoFirstFrame(streamID: String) {
                    super.onPlayerRecvVideoFirstFrame(streamID)
                    Log.i(TAG, "onPlayerRecvVideoFirstFrame: ")
                }

                override fun onPlayerRenderVideoFirstFrame(streamID: String) {
                    super.onPlayerRenderVideoFirstFrame(streamID)
                    Log.i(TAG, "onPlayerRenderVideoFirstFrame: ")
                    }

                override fun onPlayerVideoSizeChanged(streamID: String, width: Int, height: Int) {
                    super.onPlayerVideoSizeChanged(streamID, width, height)
                    Log.i(TAG, "onPlayerVideoSizeChanged: ")
                }

                override fun onPlayerRecvSEI(streamID: String, data: ByteArray) {
                    super.onPlayerRecvSEI(streamID, data)
                    Log.i(TAG, "onPlayerRecvSEI: ")
                }

                override fun onMixerRelayCDNStateUpdate(
                    taskID: String,
                    infoList: ArrayList<ZegoStreamRelayCDNInfo>
                ) {
                    super.onMixerRelayCDNStateUpdate(taskID, infoList)
                    Log.i(TAG, "onMixerRelayCDNStateUpdate: ")
                }

                override fun onMixerSoundLevelUpdate(soundLevels: HashMap<Int, Float>) {
                    super.onMixerSoundLevelUpdate(soundLevels)
                    Log.i(TAG, "onMixerSoundLevelUpdate: ")
                }

                override fun onCapturedSoundLevelUpdate(soundLevel: Float) {
                    super.onCapturedSoundLevelUpdate(soundLevel)
                    Log.i(TAG, "onCapturedSoundLevelUpdate: ")
                }

                override fun onRemoteSoundLevelUpdate(soundLevels: HashMap<String, Float>) {
                    super.onRemoteSoundLevelUpdate(soundLevels)
                    Log.i(TAG, "onRemoteSoundLevelUpdate: ")
                }

                override fun onCapturedAudioSpectrumUpdate(audioSpectrum: FloatArray) {
                    super.onCapturedAudioSpectrumUpdate(audioSpectrum)
                    Log.i(TAG, "onCapturedAudioSpectrumUpdate: ")
                }

                override fun onRemoteAudioSpectrumUpdate(audioSpectrums: HashMap<String, FloatArray>) {
                    super.onRemoteAudioSpectrumUpdate(audioSpectrums)
                    Log.i(TAG, "onRemoteAudioSpectrumUpdate: ")
                }

                override fun onDeviceError(errorCode: Int, deviceName: String) {
                    super.onDeviceError(errorCode, deviceName)
                    Log.i(TAG, "onDeviceError: ")
                }

                override fun onRemoteCameraStateUpdate(
                    streamID: String,
                    state: ZegoRemoteDeviceState
                ) {
                    super.onRemoteCameraStateUpdate(streamID, state)
                    Log.i(TAG, "onRemoteCameraStateUpdate: ")
                }

                override fun onRemoteMicStateUpdate(
                    streamID: String,
                    state: ZegoRemoteDeviceState
                ) {
                    super.onRemoteMicStateUpdate(streamID, state)
                    Log.i(TAG, "onRemoteMicStateUpdate: ")
                }

                override fun onAudioRouteChange(audioRoute: ZegoAudioRoute) {
                    super.onAudioRouteChange(audioRoute)
                    Log.i(TAG, "onAudioRouteChange: ")
                }

                override fun onIMRecvBroadcastMessage(
                    roomID: String,
                    messageList: ArrayList<ZegoBroadcastMessageInfo>
                ) {
                    super.onIMRecvBroadcastMessage(roomID, messageList)

                    Log.i(TAG, "onIMRecvBroadcastMessage: " + messageList.size)
                    Log.i(TAG, "onIMRecvBroadcastMessage: " + messageList[0].message)

                    for (i in messageList.indices) {
                        var zegoUser: ZegoUser = ZegoUser(
                            messageList[i].fromUser.userID,
                            messageList[i].fromUser.userName
                        )
                        val info: ModelBroadcastMessage = ModelBroadcastMessage(
                            messageList[i].message,
                            messageList[i].messageID,
                            messageList[i].sendTime,
                            zegoUser
                        )
                        listMessages.add(info)
                    }
                    setUpMessagesRecycler()
                    rvPlayerMessages.scrollToPosition(listMessages.size - 1)
                }

                override fun onIMRecvBarrageMessage(
                    roomID: String,
                    messageList: ArrayList<ZegoBarrageMessageInfo>
                ) {
                    super.onIMRecvBarrageMessage(roomID, messageList)
                    Log.i(TAG, "onIMRecvBarrageMessage: ")
                }

                override fun onIMRecvCustomCommand(
                    roomID: String,
                    fromUser: ZegoUser,
                    command: String
                ) {
                    super.onIMRecvCustomCommand(roomID, fromUser, command)
                    Log.i(TAG, "onIMRecvCustomCommand: ")
                }

                override fun onPerformanceStatusUpdate(status: ZegoPerformanceStatus) {
                    super.onPerformanceStatusUpdate(status)
                    Log.i(TAG, "onPerformanceStatusUpdate: ")
                }

                override fun onNetworkModeChanged(mode: ZegoNetworkMode) {
                    super.onNetworkModeChanged(mode)
                    Log.i(TAG, "onNetworkModeChanged: ")
                }

                override fun onNetworkSpeedTestError(
                    errorCode: Int,
                    type: ZegoNetworkSpeedTestType
                ) {
                    super.onNetworkSpeedTestError(errorCode, type)
                    Log.i(TAG, "onNetworkSpeedTestError: ")
                }

                override fun onNetworkSpeedTestQualityUpdate(
                    quality: ZegoNetworkSpeedTestQuality,
                    type: ZegoNetworkSpeedTestType
                ) {
                    super.onNetworkSpeedTestQualityUpdate(quality, type)
                    Log.i(TAG, "onNetworkSpeedTestQualityUpdate: ")
                }
            })

    }

    override fun onStart() {
        super.onStart()

    }

    var isPlayingLive = true
    override fun onBackPressed() {
        if (isPlayingLive) {
            stopPlaying()
        }
        super.onBackPressed()
    }

    override fun onDestroy() {
        if (isPlayingLive) {
            stopPlaying()
        }
        super.onDestroy()
    }

    fun stopPlaying() {
        if (streamID != null) {
            engine.stopPlayingStream(streamID)
        }
        engine.logoutRoom(ROOM_NAME)
        engine.setEventHandler(null)
        resourceMode = ZegoStreamResourceMode.DEFAULT
        ZegoExpressEngine.destroyEngine(null)
        isPlayingLive = false
        showToast("Live streaming ended")
    }

    lateinit var linearLayoutManagerMessages: LinearLayoutManager
    lateinit var adapterMessages: AdapterMessages
    private fun setUpMessagesRecycler() {
        linearLayoutManagerMessages =
            LinearLayoutManager(this@PlayStreamActivity, LinearLayoutManager.VERTICAL, false)
        rvPlayerMessages.layoutManager = linearLayoutManagerMessages
        adapterMessages = AdapterMessages(this@PlayStreamActivity, listMessages, this)
        rvPlayerMessages.apply {
            adapter = adapterMessages
        }
    }

    fun onPlayerSendMessageClicked(view: View) {
        var message = etPlayerSendMessage.text.toString()
        if (message.isNullOrEmpty()) {
            showToast("Message cannot be empty")
        } else {
            sendBroadcastMessage(message)
        }
    }

    fun sendBroadcastMessage(message: String) {
        engine.sendBroadcastMessage(
            ROOM_NAME, message
        ) { errorCode, messageID ->

            if (errorCode == 0) {
                var zegoUser: ZegoUser = ZegoUser(
                    user.userID,
                    user.userName
                )
                val info: ModelBroadcastMessage = ModelBroadcastMessage(
                    message,
                    messageID,
                    System.currentTimeMillis(),
                    zegoUser
                )
                listMessages.add(info)
                setUpMessagesRecycler()
                rvPlayerMessages.scrollToPosition(listMessages.size - 1)
                Log.i(TAG, "sendBroadcastMessage: ${adapterMessages.itemCount}")
                etPlayerSendMessage.setText("")
                showToast("Message sent successfully")
            }
            Log.i(TAG, "sendBroadcastMessage: $messageID + errorCode- $errorCode")
        }
    }


    override fun onMessageListItemClicked(position: Int, messageInfo: ModelBroadcastMessage) {

    }
}