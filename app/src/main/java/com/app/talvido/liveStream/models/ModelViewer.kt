package com.app.talvido.liveStream.models

import com.app.talvido.utils.NoArg

@NoArg
data class  ModelViewer(
    var name: String,
    var userId: String,
    var image: String,
    var isViewingLive:Boolean
)
