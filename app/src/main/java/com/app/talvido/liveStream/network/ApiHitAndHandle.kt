package com.app.talvido.liveStream.network

import android.app.AlertDialog
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.util.Log
import android.view.Window
import android.widget.Toast
import com.app.talvido.R
import com.app.talvido.framwork.presentation.navHost.MainActivity
import com.app.talvido.liveStream.LiveBaseActivity
import com.google.gson.JsonObject
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ApiHitAndHandle : Callback<JsonObject> {

    private var progressDialog: ProgressDialog? = null
    private lateinit var apiResponse: ApiResponseLive
    var isProgressShowing: Boolean = true
    private lateinit var context: Context

    fun hitApi(
        call: Call<JsonObject>,
        showProgress: Boolean,
        context: Context,
        listener: ApiResponseLive, IsNetwork: Boolean
    ) {
        this.context = context
        if (IsNetwork) {
            isProgressShowing = showProgress
            if (showProgress) {
                startProgressDialog(context) ///////Show Progress Dialog

            }
            call.enqueue(this)
            apiResponse = listener
        } else {
            Toast.makeText(context,  context.getString(R.string.internet), Toast.LENGTH_SHORT).show()

        }
    }

    override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
        if (isProgressShowing) {
            stopProgressDialog()
        }

        if (response.isSuccessful) {
            Log.e("response>>", response.body().toString())
            apiResponse.onSuccess(call, response.code(), response.body()!!.toString())
        } else {
            val jsonObject = JSONObject(response.errorBody()!!.string())
            val message = jsonObject.optString("message")

            Log.e("response>>", response.code().toString() + " " + response.message())
            val error = ErrorUtils(context).parseError(response)
            apiResponse.onError(call, response.code(), error.message().toString())

            when {
                response.code() == 401 -> {
                    oneButtonDialogone(context, context.getString(R.string.session_expired), context.getString(R.string.ok),
                        DialogInterface.OnClickListener { dialogInterface, i ->
                            dialogInterface.dismiss()
                            (context as LiveBaseActivity).logoutFromApp()
                        }).show()
                }
                response.code() == 426 -> {
                    oneButtonDialogone(context, message,  context.getString(R.string.ok),
                        DialogInterface.OnClickListener { dialogInterface, i ->
                            dialogInterface.dismiss()
                        }).show()
                }
                else -> {
                    oneButtonDialogone(context, message,  context.getString(R.string.ok),
                        DialogInterface.OnClickListener { dialogInterface, i -> dialogInterface.dismiss() }).show()
                }
            }
        }
    }

    override fun onFailure(call: Call<JsonObject>, t: Throwable) {
        Log.e("exception>>", t.message!!)
        if (isProgressShowing) {
            stopProgressDialog()
        }
        if (t.message == "Canceled" || t.message == "Socket closed") {

        } else {
            oneButtonDialogone(context, context.getString(R.string.server_not_responding),  context.getString(R.string.ok),
                DialogInterface.OnClickListener { dialogInterface, i -> dialogInterface.dismiss() }).show()
        }
    }

    fun oneButtonDialogone(
        c: Context,
        msg: String,
        buttonTxt: String,
        clickListener: DialogInterface.OnClickListener
    ): Dialog {
        val dialog = AlertDialog.Builder(c)
        dialog.setMessage(msg)
        dialog.setCancelable(false)
        dialog.setNeutralButton(buttonTxt, clickListener)
        return dialog.create()
    }

    fun startProgressDialog(context: Context) {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
        }

        progressDialog = ProgressDialog(context, R.style.DialogUpTheme)
        progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog!!.show()
        progressDialog!!.setCancelable(false)
    }

    fun stopProgressDialog() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
        }
    }
}