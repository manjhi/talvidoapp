package com.app.talvido.liveStream.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.app.talvido.liveStream.models.ModelViewer
import com.app.talvido.R
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_user_viewing.view.*

class AdapterViewersPlayer(
        var context: Context,
        var listUser: ArrayList<ModelViewer>,
        var userclick: UserClickcallBack
) : RecyclerView.Adapter<AdapterViewersPlayer.MyViewHolder>() {

    interface UserClickcallBack {
        fun onViewerListItemClicked(position: Int, user: ModelViewer)
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_user_viewing, parent, false)
        return MyViewHolder(view)
    }

    private var lastAnimatedPosition = -1
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.tvNameViewer.text = listUser[position].name
        Glide.with(context).load(R.drawable.ic_launcher_background).into(holder.itemView.imgViewer)

        holder.itemView.setOnClickListener { userclick.onViewerListItemClicked(
            position,
            listUser[position]
        ) }

        Log.i("liveActivity", "onBindViewHolder: " + listUser[position].name)

        if (position > lastAnimatedPosition) {
            lastAnimatedPosition = position
            val animation = AnimationUtils.loadAnimation(context, R.anim.in_from_right)
            animation.interpolator = AccelerateDecelerateInterpolator()
            holder.itemView.animation = animation
            animation.start()
        }
    }

    override fun getItemCount(): Int = listUser.size
}