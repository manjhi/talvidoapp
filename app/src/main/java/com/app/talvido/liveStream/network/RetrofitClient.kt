package com.app.talvido.liveStream.network

import android.content.Context
import android.util.Log
import com.app.talvido.business.localData.PrefsManager
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitClient(val context: Context) {
    private val CACHE_CONTROL = "Cache-Control"


    private var retrofit: Retrofit? = null


    private var okHttpClient: OkHttpClient? = null

    fun getClient(): Retrofit {
        if (okHttpClient == null)
            initOkHttp()

        retrofit = Retrofit.Builder()
            .baseUrl("http://18.224.157.23/app/index.php/api/TalvidoApis/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient!!)
            .build()

        return retrofit!!
    }

    private fun initOkHttp() {
        val httpClient = OkHttpClient().newBuilder()
            .connectTimeout(240, TimeUnit.SECONDS)
            .readTimeout(240, TimeUnit.SECONDS)
            .writeTimeout(240, TimeUnit.SECONDS)

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        httpClient.addInterceptor(interceptor)

        httpClient.addInterceptor { chain ->
            val original = chain.request()
            val requestBuilder = original.newBuilder()
            val authKey = PrefsManager.get().getString(PrefsManager.PREF_AUTH_TOKEN, null)
            Log.i("authKey", "initOkHttp: $authKey")
            if (authKey != null) {
                Log.e("Token", authKey)
                requestBuilder.addHeader("AUTHORIZATION", authKey)
            }
            val request = requestBuilder.build()
            chain.proceed(request)
        }

        okHttpClient = httpClient.build()
    }

}