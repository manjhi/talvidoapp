package com.app.talvido.liveStream.firebase

import android.content.Context
import android.util.Log
import com.app.talvido.base.delegate.MyApplication
import com.app.talvido.liveStream.models.ModelViewer
import com.app.talvido.utils.Constants
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.FirebaseDatabase
import java.util.HashMap

class FirebaseListeners(var context: Context) {

    companion object {
        const val TAG: String = "firebaseListener"
        const val VIEWERS: String = "Viewers"

        //All users
        internal var mFirebaseUsers =
            FirebaseDatabase.getInstance().reference.child(Constants.USERS)

        //All live users are in this reference
        internal var mFirebaseLiveUsers =
            FirebaseDatabase.getInstance().reference.child(Constants.LIVE_USERS)

        //        // Gifts on particular live room reference
//        internal var mFirebaseLiveUserGifts =
//            FirebaseDatabase.getInstance().reference.child(Constants.LIVE_USERS).child(liveUserId)
//                .child(Constants.LIVE_GIFTS)
//
//        //Total viewers on particular live room reference
        internal var mFirebaseLiveUserViewers =
            FirebaseDatabase.getInstance().reference.child(Constants.LIVE_USERS)

        //All Gifts and its values
        internal var mFirebaseGiftsList =
            FirebaseDatabase.getInstance().reference.child(Constants.GIFTS_LIST)

        internal var usersListener = HashMap<String, ChildEventListener>()
        internal var liveUsersListener = HashMap<String, ChildEventListener>()
        internal var giftsListener = HashMap<String, ChildEventListener>()

        fun addNewUser(modelViewer: ModelViewer) {
            mFirebaseUsers.child(modelViewer.userId).setValue(modelViewer).addOnSuccessListener {
                Log.i(TAG, "addNewUser:  User Added Successfully!")
                Log.i(TAG, "addNewUser:  ${modelViewer.name}")
            }.addOnFailureListener {
                Log.i(TAG, "addNewUser: exception ${it.message}")
            }
        }

        fun liveStartedFBNotify(userId: String, userName: String) {
            mFirebaseLiveUsers.child(userId).setValue(userName).addOnSuccessListener {
                MyApplication.getPostValueListener()!!.onValuePosted("Live Going")
            }.addOnFailureListener {
                MyApplication.getPostValueListener()!!.onValueFailure(it)
            }
        }

        fun addViewerUser(streamId: String, modelViewer: ModelViewer, myId: String) {
            mFirebaseLiveUsers.child(streamId)
                .child(Constants.LIVE_VIEWERS).child(myId).setValue(modelViewer)
                .addOnSuccessListener {
                    MyApplication.getPostValueListener()!!.onValuePosted("Live Viewer")
                }.addOnFailureListener {
                    MyApplication.getPostValueListener()!!.onValueFailure(it)
                }
        }

        fun viewersListener(streamId: String, childListener: ChildEventListener) {
            mFirebaseLiveUsers.child(streamId).child(VIEWERS).addChildEventListener(childListener)
        }

        fun removeViewerListener(streamId: String, childListener: ChildEventListener) {
            mFirebaseLiveUsers.child(streamId).child(VIEWERS).removeEventListener(childListener)
        }

        fun changeViewerStatus(streamId: String, myId: String, modelViewer: ModelViewer) {
            mFirebaseLiveUsers.child(streamId).child(VIEWERS).child(myId).setValue(modelViewer)
        }

        fun removeViewerFromFb(streamId: String, myId: String) {
            mFirebaseLiveUsers.child(streamId).child(VIEWERS).child(myId).removeValue()
        }
    }

}