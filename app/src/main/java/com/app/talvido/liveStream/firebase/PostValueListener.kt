package com.app.talvido.liveStream.firebase

import java.lang.Exception

public interface PostValueListener {
    fun onValuePosted(from:String)
    fun onValueFailure(e: Exception)
}