package com.app.talvido.liveStream.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.talvido.R
import com.app.talvido.liveStream.models.ModelUser
import kotlinx.android.synthetic.main.activity_play_stream.view.*

class AdapterPlayerRecyclerSlide(
    val context: Context,
    var listLiveUser: ArrayList<ModelUser>,
    var callBacks: PlayerRecyclerCallBacks
) : RecyclerView.Adapter<AdapterPlayerRecyclerSlide.MyViewHolder>() {


    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(context).inflate(R.layout.activity_play_stream, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.imgPlayerSendMessage.setOnClickListener {
            callBacks.sendMessage(position, holder.itemView.etPlayerSendMessage.text.toString())
        }
    }

    override fun getItemCount(): Int = listLiveUser.size

    interface PlayerRecyclerCallBacks {
        fun sendMessage(position: Int, msg: String)
    }
}




