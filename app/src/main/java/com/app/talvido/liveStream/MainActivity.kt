package com.app.talvido.liveStream

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.app.talvido.R
import com.app.talvido.liveStream.firebase.FirebaseListeners
import com.app.talvido.liveStream.models.ModelListUser
import com.app.talvido.liveStream.models.ModelUser
import com.app.talvido.liveStream.models.ModelViewer
import com.app.talvido.liveStream.playerRecycler.PlayerRecyclerActivity
import com.app.talvido.liveStream.publisher.PublishActivity
import com.app.talvido.utils.Constants
import com.app.talvido.utils.putExtraJson

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val permissionNeeded = arrayOf(
            "android.permission.CAMERA",
            "android.permission.RECORD_AUDIO"
        )

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    "android.permission.CAMERA"
                ) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(
                    this,
                    "android.permission.RECORD_AUDIO"
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(permissionNeeded, 101)
            }
        }

        //TODO
        //Add Update user on firebase users
        var modelViewer: ModelViewer = ModelViewer("Manav Mittal", "9987654", "",false)
        FirebaseListeners.addNewUser(modelViewer)
    }

    fun movetonext1(view: View) {
        startActivity(Intent(this@MainActivity, PublishActivity::class.java))
    }

    fun movetoPlay(view: View) {
        var userList: ArrayList<ModelUser> = ArrayList()
        var modelUser1: ModelUser = ModelUser("LiveRoom1", "Manav Mittal", "manav123")
        var modelUser2: ModelUser = ModelUser("LiveRoom2", "Bagicha Singh", "bagicha123")
        var modelUser3: ModelUser = ModelUser("LiveRoom3", "Manjinder Singh", "manjinder123")
        var modelUser4: ModelUser = ModelUser("LiveRoom4", "Sawan Dhiman", "sawan123")
        userList.add(modelUser1)
        userList.add(modelUser2)
        userList.add(modelUser3)
        userList.add(modelUser4)
        var modelListUser: ModelListUser = ModelListUser(userList)
        val intent = Intent(this@MainActivity, PlayerRecyclerActivity::class.java)
        intent.putExtraJson(Constants.LIVE_USER_DATA, modelListUser)
        startActivity(intent)
    }
}