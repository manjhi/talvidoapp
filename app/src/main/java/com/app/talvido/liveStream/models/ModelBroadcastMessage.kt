package com.app.talvido.liveStream.models

import im.zego.zegoexpress.entity.ZegoUser

data class ModelBroadcastMessage(
    var message:String,
    var messageID:Long,
    var sendTime:Long,
    var fromUser:ZegoUser
)
