package com.app.talvido.liveStream.network

import android.content.Context
import retrofit2.Response
import java.io.IOException

class ErrorUtils(val context: Context) {
    fun parseError(response: Response<*>): APIError {
        val converter = RetrofitClient(context).getClient().responseBodyConverter<APIError>(
            APIError::class.java, arrayOfNulls<Annotation>(0))
        val error: APIError

        try {
            error = converter.convert(response.errorBody())!!
        } catch (e: IOException) {
            return APIError()
        }
        return error
    }
}