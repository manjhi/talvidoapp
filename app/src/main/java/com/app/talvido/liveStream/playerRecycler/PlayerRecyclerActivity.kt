package com.app.talvido.liveStream.playerRecycler

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.TextureView
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.*
import com.app.talvido.liveStream.LiveBaseActivity
import com.app.talvido.liveStream.adapter.AdapterPlayerRecyclerSlide
import com.app.talvido.liveStream.adapter.AdapterMessages
import com.app.talvido.liveStream.adapter.AdapterViewersPlayer
import com.app.talvido.liveStream.firebase.FirebaseListeners
import com.app.talvido.liveStream.firebase.PostValueListener
import com.app.talvido.liveStream.models.ModelBroadcastMessage
import com.app.talvido.liveStream.models.ModelListUser
import com.app.talvido.liveStream.models.ModelUser
import com.app.talvido.liveStream.models.ModelViewer
import com.app.talvido.utils.Constants
import com.app.talvido.utils.getJsonExtra
import com.app.talvido.R
import com.app.talvido.base.delegate.MyApplication
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import im.zego.zegoexpress.ZegoExpressEngine
import im.zego.zegoexpress.callback.IZegoEventHandler
import im.zego.zegoexpress.constants.*
import im.zego.zegoexpress.entity.*
import kotlinx.android.synthetic.main.activity_play_stream.view.*
import kotlinx.android.synthetic.main.activity_player_recycler.*
import org.json.JSONObject
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

class PlayerRecyclerActivity : LiveBaseActivity(),
    AdapterPlayerRecyclerSlide.PlayerRecyclerCallBacks, AdapterMessages.MessageCallback,
    AdapterViewersPlayer.UserClickcallBack, PostValueListener {
    lateinit var engine: ZegoExpressEngine
    val TAG = "PlayerliveActivity"
    var listMessages: ArrayList<ModelBroadcastMessage> = ArrayList()
    val user = ZegoUser("436827190", "Bagicha Singh")
    var currentRecyclerPosition: Int = 0
    var currentItems = 0
    var totalItem: Int = 0
    var scrollOutItems: Int = 0
    var page: Int = 0
    var currentPage = -1
    var ROOM_NAME: String = ""
    private var zegoCanvas: ZegoCanvas? = null
    lateinit var rvViewersPlayer: RecyclerView
    lateinit var rvPlayerMessages: RecyclerView
    lateinit var etPlayerSendMessage: EditText
    lateinit var play_view: TextureView
    lateinit var tvMyName: TextView
    var viewMode = ZegoViewMode.ASPECT_FILL
    private var resourceMode = ZegoStreamResourceMode.DEFAULT
    private var streamID = ""
    var listLiveUser: ArrayList<ModelUser> = ArrayList()
    var lastPosition: Int = 0
    var listViewersPlayer: ArrayList<ModelViewer> = ArrayList()
    lateinit var adapterMessages: AdapterMessages
    lateinit var adapterViewersPlayer: AdapterViewersPlayer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_player_recycler)

        var modelListUser: ModelListUser = intent!!.getJsonExtra(
            Constants.LIVE_USER_DATA,
            ModelListUser::class.java
        ) as ModelListUser

        listLiveUser = modelListUser.list
        ROOM_NAME = listLiveUser[0].roomName
        streamID = listLiveUser[0].streamId

        initEngine()
        setUpRecycler()
        MyApplication.setPostValueListener(this)
        if (engine != null) {
            Toast.makeText(this, "Engine Initailized", Toast.LENGTH_SHORT).show()
        }
    }

    lateinit var adapterPlayerRecyclerSlide: AdapterPlayerRecyclerSlide
    lateinit var layoutManager: LinearLayoutManager

    private fun setUpRecycler() {
        layoutManager =
            LinearLayoutManager(this@PlayerRecyclerActivity, LinearLayoutManager.VERTICAL, false)
        val snapHelper: SnapHelper = PagerSnapHelper()
        recycler_player.itemAnimator = DefaultItemAnimator()
        snapHelper.attachToRecyclerView(recycler_player)
        recycler_player.layoutManager = layoutManager

        adapterPlayerRecyclerSlide =
            AdapterPlayerRecyclerSlide(
                this@PlayerRecyclerActivity,
                listLiveUser,
                this
            )
        recycler_player.adapter = adapterPlayerRecyclerSlide

        recycler_player.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                //here we find the current item number
                currentItems = layoutManager.getChildCount()
                totalItem = layoutManager.getItemCount()
                scrollOutItems = layoutManager.findFirstVisibleItemPosition()

                val scrollOffset = recyclerView.computeVerticalScrollOffset()
                val height = recyclerView.height
                val page_no = scrollOffset / height
                if (page_no != currentPage) {

                    currentPage = page_no
                    if (lastPosition != currentPage) {
                        Log.i(
                            TAG,
                            "onScrolled: last position $lastPosition currentPosition== $currentPage"
                        )
                        stopPlaying()
                        lastPosition = currentPage
                    }

                    currentRecyclerPosition = currentPage
                    Log.i(TAG, "onScrolled: ROOM_NAME--- $ROOM_NAME    ---  streamID--- $streamID ")

                    initRecyclerItems(currentPage!!)

                    listMessages = ArrayList()
                    listViewersPlayer = ArrayList()
                    setUpMessages(listMessages)
                    setUpViewers(listViewersPlayer)

                    Log.i(TAG, "onScrolled: $page streamId- $streamID roomName- $ROOM_NAME")

                    Handler().postDelayed(Runnable {
                        engine.loginRoom(ROOM_NAME, user, null)     //login into current user's room
                        zegoCanvas = ZegoCanvas(play_view)
                        zegoCanvas!!.viewMode = viewMode
                        val playerConfig = ZegoPlayerConfig()
                        playerConfig.resourceMode = resourceMode
                        engine.startPlayingStream(streamID, zegoCanvas, playerConfig)

                        Log.i(TAG, "onScrolled: publishing started")
                    }, 1000)
                }
            }
        })
    }

    private fun addViewerOnFirebase(modelViewer: ModelViewer, streamID: String) {
        Log.i(TAG, "addViewerOnFirebase: ${modelViewer.name}")
        FirebaseListeners.addViewerUser(streamID, modelViewer, user.userID)
    }

    private fun removeViewerFromFirebase() {
        Log.i(TAG, "removeViewerFromFirebase: ")
        FirebaseListeners.removeViewerFromFb(streamID, user.userID)
    }

    private fun changeStatus() {
        var modelViewer = ModelViewer(user.userName, user.userID, "", false)
        FirebaseListeners.changeViewerStatus(streamID, user.userID, modelViewer)
    }

    private fun setViewersListener(streamID: String) {
        Log.i(TAG, "setViewersListener: Listener added- $streamID")
        FirebaseListeners.viewersListener(streamID, liveViewersListener)
    }

    private fun removeViewersListener(streamId: String) {
        Log.i(TAG, "removeViewersListener: Listener Removed - streadId- $streamId")
        FirebaseListeners.removeViewerListener(streamId, liveViewersListener)
    }

    private var liveViewersListener = object : ChildEventListener {
        override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
            Log.i(TAG, "onChildAdded: Snapshot Id- ${snapshot.key}")

            var modelViewer: ModelViewer = snapshot.getValue(ModelViewer::class.java)!!
            if (modelViewer.isViewingLive) {
                listViewersPlayer.add(modelViewer)
                adapterViewersPlayer.notifyDataSetChanged()
            }
        }

        override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
            var modelViewer: ModelViewer = snapshot.getValue(ModelViewer::class.java)!!
            if (!modelViewer.isViewingLive) {
                for (index in listViewersPlayer.indices) {
                    if (listViewersPlayer[index].userId.equals(modelViewer.userId)) {
                        listViewersPlayer.removeAt(index)
                        adapterViewersPlayer.notifyItemRemoved(index)
                        break
                    }
                }
            } else {
                listViewersPlayer.add(modelViewer)
                adapterViewersPlayer.notifyDataSetChanged()
            }
        }

        override fun onChildRemoved(snapshot: DataSnapshot) {

        }

        override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {

        }

        override fun onCancelled(error: DatabaseError) {

        }

    }

    private fun initRecyclerItems(currentPage: Int) {
        val view: View = layoutManager.findViewByPosition(currentPage)!!
        tvMyName = view.findViewById(R.id.tvMyName)
        rvViewersPlayer = view.findViewById(R.id.rvViewersPlayer)
        rvPlayerMessages = view.findViewById(R.id.rvPlayerMessages)
        etPlayerSendMessage = view.findViewById(R.id.etPlayerSendMessage)
        play_view = view.findViewById(R.id.play_view)

        ROOM_NAME = listLiveUser[currentPage].roomName
        streamID = listLiveUser[currentPage].streamId
    }

    private fun setUpViewers(listViewersPlayer: ArrayList<ModelViewer>) {
        adapterViewersPlayer =
            AdapterViewersPlayer(this@PlayerRecyclerActivity, listViewersPlayer, this)

        rvViewersPlayer.adapter = adapterViewersPlayer

    }

    private fun setUpMessages(listMessages: ArrayList<ModelBroadcastMessage>) {
        adapterMessages =
            AdapterMessages(this@PlayerRecyclerActivity, listMessages, this)
        rvPlayerMessages.adapter = adapterMessages
    }

    private fun initEngine() {
        engine = ZegoExpressEngine.createEngine(
            Constants.appID, Constants.appSign, true, ZegoScenario.GENERAL,
            application, zegoEventHandler()
        )
    }

    private fun zegoEventHandler(): IZegoEventHandler {
        var zegoEventHandler: IZegoEventHandler = object : IZegoEventHandler() {
            override fun onEngineStateUpdate(state: ZegoEngineState) {
                super.onEngineStateUpdate(state)
            }

            override fun onRoomStateUpdate(
                roomID: String,
                state: ZegoRoomState,
                errorCode: Int,
                extendedData: JSONObject
            ) {
                super.onRoomStateUpdate(roomID, state, errorCode, extendedData)
                Log.i(TAG, "onRoomStateUpdate: $roomID $state")
                if (state == ZegoRoomState.DISCONNECTED) {
//                    onBackPressed()
                } else if (state == ZegoRoomState.CONNECTED) {
                    //add live viewer on firebase in the list of live publisher
                    var modelViewer: ModelViewer = ModelViewer(user.userName, user.userID, "", true)
                    addViewerOnFirebase(modelViewer, streamID)
                    setViewersListener(streamID)
                }

            }

            override fun onRoomUserUpdate(
                roomID: String,
                updateType: ZegoUpdateType,
                userList: java.util.ArrayList<ZegoUser>
            ) {
                super.onRoomUserUpdate(roomID, updateType, userList)
                Log.i(TAG, "onRoomUserUpdate: ")

//                for (value in userList) {
//                    if (updateType == ZegoUpdateType.ADD) {
//                        listViewersPlayer.add(value)
//                    } else {
//                        listViewersPlayer.remove(value)
//                    }
//                    adapterViewersPlayer.notifyDataSetChanged()
//                }
            }

            override fun onRoomOnlineUserCountUpdate(roomID: String, count: Int) {
                super.onRoomOnlineUserCountUpdate(roomID, count)
                Log.i(TAG, "onRoomOnlineUserCountUpdate: ")
            }

            override fun onRoomStreamUpdate(
                roomID: String,
                updateType: ZegoUpdateType,
                streamList: java.util.ArrayList<ZegoStream>
            ) {
                super.onRoomStreamUpdate(roomID, updateType, streamList)
                Log.i(TAG, "onRoomStreamUpdate: ")

            }

            override fun onRoomStreamUpdate(
                roomID: String,
                updateType: ZegoUpdateType,
                streamList: java.util.ArrayList<ZegoStream>,
                extendedData: JSONObject
            ) {
                super.onRoomStreamUpdate(roomID, updateType, streamList, extendedData)
                Log.i(
                    TAG,
                    "onRoomStreamUpdate: ${extendedData.toString()}   ${streamList.size}  ${updateType}"
                )

                if (updateType == ZegoUpdateType.DELETE) {
                    onBackPressed()
                }

            }

            override fun onRoomStreamExtraInfoUpdate(
                roomID: String,
                streamList: java.util.ArrayList<ZegoStream>
            ) {
                super.onRoomStreamExtraInfoUpdate(roomID, streamList)
                Log.i(TAG, "onRoomStreamExtraInfoUpdate: ")

            }

            override fun onRoomExtraInfoUpdate(
                roomID: String,
                roomExtraInfoList: java.util.ArrayList<ZegoRoomExtraInfo>
            ) {
                super.onRoomExtraInfoUpdate(roomID, roomExtraInfoList)
                Log.i(TAG, "onRoomExtraInfoUpdate: ")
            }

            override fun onPublisherStateUpdate(
                streamID: String,
                state: ZegoPublisherState,
                errorCode: Int,
                extendedData: JSONObject
            ) {
                super.onPublisherStateUpdate(streamID, state, errorCode, extendedData)
                Log.i(TAG, "onPublisherStateUpdate: ")
            }

            override fun onPublisherQualityUpdate(
                streamID: String,
                quality: ZegoPublishStreamQuality
            ) {
                super.onPublisherQualityUpdate(streamID, quality)
                Log.i(TAG, "onPublisherQualityUpdate: ")
            }

            override fun onPublisherCapturedAudioFirstFrame() {
                super.onPublisherCapturedAudioFirstFrame()
                Log.i(TAG, "onPublisherCapturedAudioFirstFrame: ")
            }

            override fun onPublisherCapturedVideoFirstFrame(channel: ZegoPublishChannel) {
                super.onPublisherCapturedVideoFirstFrame(channel)
                Log.i(TAG, "onPublisherCapturedVideoFirstFrame: ")
            }

            override fun onPublisherVideoSizeChanged(
                width: Int,
                height: Int,
                channel: ZegoPublishChannel
            ) {
                super.onPublisherVideoSizeChanged(width, height, channel)
                Log.i(TAG, "onPublisherVideoSizeChanged: ")
            }

            override fun onPublisherRelayCDNStateUpdate(
                streamID: String,
                infoList: java.util.ArrayList<ZegoStreamRelayCDNInfo>
            ) {
                super.onPublisherRelayCDNStateUpdate(streamID, infoList)
                Log.i(TAG, "onPublisherRelayCDNStateUpdate: ")
            }

            override fun onPlayerStateUpdate(
                streamID: String,
                state: ZegoPlayerState,
                errorCode: Int,
                extendedData: JSONObject
            ) {
                super.onPlayerStateUpdate(streamID, state, errorCode, extendedData)
                Log.i(TAG, "onPlayerStateUpdate: " + state)

            }

            override fun onPlayerQualityUpdate(
                streamID: String,
                quality: ZegoPlayStreamQuality
            ) {
                super.onPlayerQualityUpdate(streamID, quality)
                Log.i(TAG, "onPlayerQualityUpdate: ")
            }

            override fun onPlayerMediaEvent(streamID: String, event: ZegoPlayerMediaEvent) {
                super.onPlayerMediaEvent(streamID, event)
                Log.i(TAG, "onPlayerMediaEvent: ")
            }

            override fun onPlayerRecvAudioFirstFrame(streamID: String) {
                super.onPlayerRecvAudioFirstFrame(streamID)
                Log.i(TAG, "onPlayerRecvAudioFirstFrame: ")
            }

            override fun onPlayerRecvVideoFirstFrame(streamID: String) {
                super.onPlayerRecvVideoFirstFrame(streamID)
                Log.i(TAG, "onPlayerRecvVideoFirstFrame: ")
            }

            override fun onPlayerRenderVideoFirstFrame(streamID: String) {
                super.onPlayerRenderVideoFirstFrame(streamID)
                Log.i(TAG, "onPlayerRenderVideoFirstFrame: ")
            }

            override fun onPlayerVideoSizeChanged(
                streamID: String,
                width: Int,
                height: Int
            ) {
                super.onPlayerVideoSizeChanged(streamID, width, height)
                Log.i(TAG, "onPlayerVideoSizeChanged: ")
            }

            override fun onPlayerRecvSEI(streamID: String, data: ByteArray) {
                super.onPlayerRecvSEI(streamID, data)
                Log.i(TAG, "onPlayerRecvSEI: ")
            }

            override fun onMixerRelayCDNStateUpdate(
                taskID: String,
                infoList: java.util.ArrayList<ZegoStreamRelayCDNInfo>
            ) {
                super.onMixerRelayCDNStateUpdate(taskID, infoList)
                Log.i(TAG, "onMixerRelayCDNStateUpdate: ")
            }

            override fun onMixerSoundLevelUpdate(soundLevels: HashMap<Int, Float>) {
                super.onMixerSoundLevelUpdate(soundLevels)
                Log.i(TAG, "onMixerSoundLevelUpdate: ")
            }

            override fun onCapturedSoundLevelUpdate(soundLevel: Float) {
                super.onCapturedSoundLevelUpdate(soundLevel)
                Log.i(TAG, "onCapturedSoundLevelUpdate: ")
            }

            override fun onRemoteSoundLevelUpdate(soundLevels: HashMap<String, Float>) {
                super.onRemoteSoundLevelUpdate(soundLevels)
                Log.i(TAG, "onRemoteSoundLevelUpdate: ")
            }

            override fun onCapturedAudioSpectrumUpdate(audioSpectrum: FloatArray) {
                super.onCapturedAudioSpectrumUpdate(audioSpectrum)
                Log.i(TAG, "onCapturedAudioSpectrumUpdate: ")
            }

            override fun onRemoteAudioSpectrumUpdate(audioSpectrums: HashMap<String, FloatArray>) {
                super.onRemoteAudioSpectrumUpdate(audioSpectrums)
                Log.i(TAG, "onRemoteAudioSpectrumUpdate: ")
            }

            override fun onDeviceError(errorCode: Int, deviceName: String) {
                super.onDeviceError(errorCode, deviceName)
                Log.i(TAG, "onDeviceError: ")
            }

            override fun onRemoteCameraStateUpdate(
                streamID: String,
                state: ZegoRemoteDeviceState
            ) {
                super.onRemoteCameraStateUpdate(streamID, state)
                Log.i(TAG, "onRemoteCameraStateUpdate: ")
            }

            override fun onRemoteMicStateUpdate(
                streamID: String,
                state: ZegoRemoteDeviceState
            ) {
                super.onRemoteMicStateUpdate(streamID, state)
                Log.i(TAG, "onRemoteMicStateUpdate: ")
            }

            override fun onAudioRouteChange(audioRoute: ZegoAudioRoute) {
                super.onAudioRouteChange(audioRoute)
                Log.i(TAG, "onAudioRouteChange: ")
            }

            override fun onIMRecvBroadcastMessage(
                roomID: String,
                messageList: java.util.ArrayList<ZegoBroadcastMessageInfo>
            ) {
                super.onIMRecvBroadcastMessage(roomID, messageList)

                Log.i(TAG, "onIMRecvBroadcastMessage: " + messageList.size)
                Log.i(TAG, "onIMRecvBroadcastMessage: " + messageList[0].message)

                for (i in messageList.indices) {
                    var zegoUser: ZegoUser = ZegoUser(
                        messageList[i].fromUser.userID,
                        messageList[i].fromUser.userName
                    )
                    val info: ModelBroadcastMessage = ModelBroadcastMessage(
                        messageList[i].message,
                        messageList[i].messageID,
                        messageList[i].sendTime,
                        zegoUser
                    )
                    listMessages.add(info)
                }
                adapterMessages.notifyDataSetChanged()
                rvPlayerMessages.scrollToPosition(listMessages.size - 1)
            }

            override fun onIMRecvBarrageMessage(
                roomID: String,
                messageList: java.util.ArrayList<ZegoBarrageMessageInfo>
            ) {
                super.onIMRecvBarrageMessage(roomID, messageList)
                Log.i(TAG, "onIMRecvBarrageMessage: ")
            }

            override fun onIMRecvCustomCommand(
                roomID: String,
                fromUser: ZegoUser,
                command: String
            ) {
                super.onIMRecvCustomCommand(roomID, fromUser, command)
                Log.i(TAG, "onIMRecvCustomCommand: ")
            }

            override fun onPerformanceStatusUpdate(status: ZegoPerformanceStatus) {
                super.onPerformanceStatusUpdate(status)
                Log.i(TAG, "onPerformanceStatusUpdate: ")
            }

            override fun onNetworkModeChanged(mode: ZegoNetworkMode) {
                super.onNetworkModeChanged(mode)
                Log.i(TAG, "onNetworkModeChanged: ")
            }

            override fun onNetworkSpeedTestError(
                errorCode: Int,
                type: ZegoNetworkSpeedTestType
            ) {
                super.onNetworkSpeedTestError(errorCode, type)
                Log.i(TAG, "onNetworkSpeedTestError: ")
            }

            override fun onNetworkSpeedTestQualityUpdate(
                quality: ZegoNetworkSpeedTestQuality,
                type: ZegoNetworkSpeedTestType
            ) {
                super.onNetworkSpeedTestQualityUpdate(quality, type)
                Log.i(TAG, "onNetworkSpeedTestQualityUpdate: ")
            }
        }
        return zegoEventHandler
    }


    private fun onPlayerSendMessageClicked(message: String) {
        if (message.isNullOrEmpty()) {
            showToast("Message cannot be empty")
        } else {
            sendBroadcastMessage(message)
        }
    }

    private fun sendBroadcastMessage(message: String) {
        engine.sendBroadcastMessage(
            ROOM_NAME, message
        ) { errorCode, messageID ->

            if (errorCode == 0) {
                var zegoUser: ZegoUser = ZegoUser(
                    user.userID,
                    user.userName
                )
                val info: ModelBroadcastMessage = ModelBroadcastMessage(
                    message,
                    messageID,
                    System.currentTimeMillis(),
                    zegoUser
                )
                listMessages.add(info)
                adapterMessages.notifyDataSetChanged()

                rvPlayerMessages.scrollToPosition(listMessages.size - 1)
                etPlayerSendMessage.setText("")
                showToast("Message sent successfully")
            }
            Log.i(TAG, "sendBroadcastMessage: $messageID + errorCode- $errorCode")
        }
    }

    var isPlayingLive = true
    override fun onBackPressed() {
        if (isPlayingLive) {
            stopPlaying()
        }
        ZegoExpressEngine.destroyEngine(null)
        super.onBackPressed()

    }

    override fun onDestroy() {
        if (isPlayingLive) {
            stopPlaying()
        }
        ZegoExpressEngine.destroyEngine(null)
        super.onDestroy()
    }

    fun stopPlaying() {
        Log.i(TAG, "stopPlaying: $")
        if (streamID != null) {
            engine.stopPlayingStream(streamID)
        }
        engine.logoutRoom(ROOM_NAME)
//        engine.setEventHandler(null)
        resourceMode = ZegoStreamResourceMode.DEFAULT
        isPlayingLive = false
        changeStatus()
        removeViewersListener(streamID)
        showToast("Live streaming ended")
        Log.i(TAG, "stopPlaying: live ended")
    }

    override fun sendMessage(position: Int, msg: String) {
        onPlayerSendMessageClicked(msg)
    }

    override fun onMessageListItemClicked(position: Int, messageInfo: ModelBroadcastMessage) {

    }

    override fun onViewerListItemClicked(position: Int, user: ModelViewer) {

    }

    override fun onValuePosted(from: String) {
        Log.i(TAG, "onValuePosted: Successfully added $from")
    }

    override fun onValueFailure(e: Exception) {
        Log.i(TAG, "onValueFailure: Viewer ${e.message}")
    }


}