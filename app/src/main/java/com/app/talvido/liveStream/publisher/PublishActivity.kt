package com.app.talvido.liveStream.publisher

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.talvido.liveStream.LiveBaseActivity
import com.app.talvido.liveStream.adapter.AdapterMessages
import com.app.talvido.liveStream.adapter.AdapterViewersPlayer
import com.app.talvido.liveStream.firebase.FirebaseListeners
import com.app.talvido.liveStream.firebase.PostValueListener
import com.app.talvido.liveStream.models.ModelBroadcastMessage
import com.app.talvido.liveStream.models.ModelViewer
import com.app.talvido.utils.Constants
import com.app.talvido.R
import com.app.talvido.base.delegate.MyApplication
import com.app.talvido.base.presentation.extensions.observe
import com.app.talvido.business.localData.PrefsManager
import com.app.talvido.framwork.datasource.model.AuthResponse
import com.app.talvido.framwork.datasource.requestModel.PublishLiveModel
import com.app.talvido.liveStream.LiveStreamService
import com.bumptech.glide.Glide
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import im.zego.zegoexpress.ZegoExpressEngine
import im.zego.zegoexpress.callback.IZegoEventHandler
import im.zego.zegoexpress.constants.*
import im.zego.zegoexpress.entity.*
import kotlinx.android.synthetic.main.activity_publish.*
import org.json.JSONObject
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList
import com.app.talvido.liveStream.customViews.CustomMinSeekBar
import com.app.talvido.liveStream.models.PublishLiveResponse
import com.app.talvido.liveStream.models.PublishResponse
import com.app.talvido.liveStream.network.ApiResponseLive
import com.app.talvido.liveStream.viewModel.PublishViewModel
import com.app.talvido.utils.CustomAlertDialog
import com.app.talvido.utils.toast
import com.google.gson.Gson
import com.google.gson.JsonObject
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.item_home_video_list.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import retrofit2.Call

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class PublishActivity : LiveBaseActivity(), AdapterViewersPlayer.UserClickcallBack,
    AdapterMessages.MessageCallback, PostValueListener, CustomAlertDialog.CallbackCustomDialog,
    ApiResponseLive {
    lateinit var engine: ZegoExpressEngine
    private var zegoCanvas: ZegoCanvas? = null
    var viewMode = ZegoViewMode.ASPECT_FILL
    private lateinit var ROOM_NAME: String
    val TAG = "liveActivity"
    private var streamID = "manav123"
    var videoConfig = ZegoVideoConfig()
    var listMessages: ArrayList<ModelBroadcastMessage> = ArrayList()
    var roomConnectionState: Boolean = false
    var listViewersPlayer: ArrayList<ModelViewer> = ArrayList()
    lateinit var authResponse: AuthResponse
    private lateinit var user: ZegoUser
    private val viewModel: PublishViewModel by viewModels()
    var liveId: String? = null
    lateinit var callGoLive: Call<JsonObject>
    lateinit var callStopLive: Call<JsonObject>

    private var API_TYPE = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_publish)

        getExtra()
        initEngine()
        setUpViewerRecycler()
        setUpMessagesRecycler()
        cameraZoom()
        MyApplication.setPostValueListener(this)  //listener that live started has been updated on firebase
        // so that viewers list can be made accordingly
        if (engine != null) {
            Toast.makeText(this, "Starting Live" + " " + user.userName, Toast.LENGTH_SHORT).show()
        }

        zegoCanvas = ZegoCanvas(preview)
        observe(viewModel.stateLiveData, observer)
        /*observe(viewModel.stateLiveData, observerStopStream)*/
    }

    private fun getExtra() {
        authResponse = Gson().fromJson(
            PrefsManager.get().getString(PrefsManager.PREF_USER_PROFILE, ""),
            AuthResponse::class.java
        ) as AuthResponse

        var username = authResponse.username
        var splittedUsername = username.removePrefix("@")
        var userImage = authResponse.image
        user = ZegoUser(authResponse.id, authResponse.name)
        streamID = authResponse.id + splittedUsername   //userid + username
        ROOM_NAME = streamID + authResponse.id               // userid + username + userid
        tvMyName.text = user.userName
        Glide.with(this@PublishActivity).load(userImage).into(imgPublisher)

        Log.i(TAG, "getExtra: userImage $userImage ")
        Log.i(TAG, "getExtra: ${authResponse.id}    ${authResponse.name}")
    }

    private fun firebase() {
        FirebaseListeners.liveStartedFBNotify(streamID, user.userName)
    }

    private fun initEngine() {
        engine = ZegoExpressEngine.createEngine(
            Constants.appID, Constants.appSign, true, ZegoScenario.GENERAL,
            application, object : IZegoEventHandler() {
                override fun onEngineStateUpdate(state: ZegoEngineState) {
                    super.onEngineStateUpdate(state)
                }

                override fun onRoomStateUpdate(
                    roomID: String,
                    state: ZegoRoomState,
                    errorCode: Int,
                    extendedData: JSONObject
                ) {
                    super.onRoomStateUpdate(roomID, state, errorCode, extendedData)
                    Log.i(TAG, "onRoomStateUpdate: $roomID $state")
                    if (state == ZegoRoomState.CONNECTED) {
                        roomConnectionState = true
                        zegoCanvas!!.viewMode = viewMode
                        engine!!.startPreview(zegoCanvas)
                        seekbarZoom.visibility = View.VISIBLE

                    }
                }

                override fun onRoomUserUpdate(
                    roomID: String,
                    updateType: ZegoUpdateType,
                    userList: ArrayList<ZegoUser>
                ) {
                    super.onRoomUserUpdate(roomID, updateType, userList)
                    Log.i(TAG, "onRoomUserUpdate: " + roomID + " list " + userList[0].userName)
//                    listViewers = userList
//                    adapterViewersPlayer!!.notifyDataSetChanged()
                }

                override fun onRoomOnlineUserCountUpdate(roomID: String, count: Int) {
                    super.onRoomOnlineUserCountUpdate(roomID, count)
                    Log.i(TAG, "onRoomOnlineUserCountUpdate: count-" + count)
//                    tvLiveCount.text = count.toString()
                }

                override fun onRoomStreamUpdate(
                    roomID: String,
                    updateType: ZegoUpdateType,
                    streamList: ArrayList<ZegoStream>
                ) {
                    super.onRoomStreamUpdate(roomID, updateType, streamList)
                    Log.i(TAG, "onRoomStreamUpdate: $updateType")
                }

                override fun onRoomStreamUpdate(
                    roomID: String,
                    updateType: ZegoUpdateType,
                    streamList: ArrayList<ZegoStream>,
                    extendedData: JSONObject
                ) {
                    super.onRoomStreamUpdate(roomID, updateType, streamList, extendedData)
                    Log.i(TAG, "onRoomStreamUpdate: $updateType")
                }

                override fun onRoomStreamExtraInfoUpdate(
                    roomID: String,
                    streamList: ArrayList<ZegoStream>
                ) {
                    super.onRoomStreamExtraInfoUpdate(roomID, streamList)
                    Log.i(TAG, "onRoomStreamExtraInfoUpdate: ${streamList[0].extraInfo}")
                }

                override fun onRoomExtraInfoUpdate(
                    roomID: String,
                    roomExtraInfoList: ArrayList<ZegoRoomExtraInfo>
                ) {
                    super.onRoomExtraInfoUpdate(roomID, roomExtraInfoList)
                    Log.i(TAG, "onRoomExtraInfoUpdate: ${roomExtraInfoList[0].value}")
                }

                override fun onPublisherStateUpdate(
                    streamID: String,
                    state: ZegoPublisherState,
                    errorCode: Int,
                    extendedData: JSONObject
                ) {
                    super.onPublisherStateUpdate(streamID, state, errorCode, extendedData)
                    Log.i(TAG, "onPublisherStateUpdate: $state   $errorCode   $extendedData")
                }

                override fun onPublisherQualityUpdate(
                    streamID: String,
                    quality: ZegoPublishStreamQuality
                ) {
                    super.onPublisherQualityUpdate(streamID, quality)
                }

                override fun onPublisherCapturedAudioFirstFrame() {
                    super.onPublisherCapturedAudioFirstFrame()
                }

                override fun onPublisherCapturedVideoFirstFrame(channel: ZegoPublishChannel) {
                    super.onPublisherCapturedVideoFirstFrame(channel)
                }

                override fun onPublisherVideoSizeChanged(
                    width: Int,
                    height: Int,
                    channel: ZegoPublishChannel
                ) {
                    super.onPublisherVideoSizeChanged(width, height, channel)
                }

                override fun onPublisherRelayCDNStateUpdate(
                    streamID: String,
                    infoList: ArrayList<ZegoStreamRelayCDNInfo>
                ) {
                    super.onPublisherRelayCDNStateUpdate(streamID, infoList)
                }

                override fun onPlayerStateUpdate(
                    streamID: String,
                    state: ZegoPlayerState,
                    errorCode: Int,
                    extendedData: JSONObject
                ) {
                    super.onPlayerStateUpdate(streamID, state, errorCode, extendedData)
                    Log.i(TAG, "onPlayerStateUpdate: $state   $errorCode   $extendedData")
                }

                override fun onPlayerQualityUpdate(
                    streamID: String,
                    quality: ZegoPlayStreamQuality
                ) {
                    super.onPlayerQualityUpdate(streamID, quality)
                }

                override fun onPlayerMediaEvent(streamID: String, event: ZegoPlayerMediaEvent) {
                    super.onPlayerMediaEvent(streamID, event)
                }

                override fun onPlayerRecvAudioFirstFrame(streamID: String) {
                    super.onPlayerRecvAudioFirstFrame(streamID)
                }

                override fun onPlayerRecvVideoFirstFrame(streamID: String) {
                    super.onPlayerRecvVideoFirstFrame(streamID)
                }

                override fun onPlayerRenderVideoFirstFrame(streamID: String) {
                    super.onPlayerRenderVideoFirstFrame(streamID)
                }

                override fun onPlayerVideoSizeChanged(streamID: String, width: Int, height: Int) {
                    super.onPlayerVideoSizeChanged(streamID, width, height)
                }

                override fun onPlayerRecvSEI(streamID: String, data: ByteArray) {
                    super.onPlayerRecvSEI(streamID, data)
                }

                override fun onMixerRelayCDNStateUpdate(
                    taskID: String,
                    infoList: ArrayList<ZegoStreamRelayCDNInfo>
                ) {
                    super.onMixerRelayCDNStateUpdate(taskID, infoList)
                }

                override fun onMixerSoundLevelUpdate(soundLevels: HashMap<Int, Float>) {
                    super.onMixerSoundLevelUpdate(soundLevels)
                }

                override fun onCapturedSoundLevelUpdate(soundLevel: Float) {
                    super.onCapturedSoundLevelUpdate(soundLevel)
                }

                override fun onRemoteSoundLevelUpdate(soundLevels: HashMap<String, Float>) {
                    super.onRemoteSoundLevelUpdate(soundLevels)
                }

                override fun onCapturedAudioSpectrumUpdate(audioSpectrum: FloatArray) {
                    super.onCapturedAudioSpectrumUpdate(audioSpectrum)
                }

                override fun onRemoteAudioSpectrumUpdate(audioSpectrums: HashMap<String, FloatArray>) {
                    super.onRemoteAudioSpectrumUpdate(audioSpectrums)
                }

                override fun onDeviceError(errorCode: Int, deviceName: String) {
                    super.onDeviceError(errorCode, deviceName)
                }

                override fun onRemoteCameraStateUpdate(
                    streamID: String,
                    state: ZegoRemoteDeviceState
                ) {
                    super.onRemoteCameraStateUpdate(streamID, state)
                }

                override fun onRemoteMicStateUpdate(
                    streamID: String,
                    state: ZegoRemoteDeviceState
                ) {
                    super.onRemoteMicStateUpdate(streamID, state)
                }

                override fun onAudioRouteChange(audioRoute: ZegoAudioRoute) {
                    super.onAudioRouteChange(audioRoute)
                }

                override fun onIMRecvBroadcastMessage(
                    roomID: String,
                    messageList: ArrayList<ZegoBroadcastMessageInfo>
                ) {
                    super.onIMRecvBroadcastMessage(roomID, messageList)
                    Log.i(TAG, "onIMRecvBroadcastMessage: " + messageList.size)
                    Log.i(TAG, "onIMRecvBroadcastMessage: " + messageList[0].message)

                    for (i in messageList.indices) {
                        var zegoUser: ZegoUser = ZegoUser(
                            messageList[i].fromUser.userID,
                            messageList[i].fromUser.userName
                        )
                        val info: ModelBroadcastMessage = ModelBroadcastMessage(
                            messageList[i].message,
                            messageList[i].messageID,
                            messageList[i].sendTime,
                            zegoUser
                        )
                        listMessages.add(info)
                    }
                    setUpMessagesRecycler()
                    rvMessages.scrollToPosition(listMessages.size - 1)
                }

                override fun onIMRecvBarrageMessage(
                    roomID: String,
                    messageList: ArrayList<ZegoBarrageMessageInfo>
                ) {
                    super.onIMRecvBarrageMessage(roomID, messageList)
                }

                override fun onIMRecvCustomCommand(
                    roomID: String,
                    fromUser: ZegoUser,
                    command: String
                ) {
                    super.onIMRecvCustomCommand(roomID, fromUser, command)
                    Log.i(TAG, "onIMRecvCustomCommand: $fromUser   $command")
                }

                override fun onPerformanceStatusUpdate(status: ZegoPerformanceStatus) {
                    super.onPerformanceStatusUpdate(status)
                }

                override fun onNetworkModeChanged(mode: ZegoNetworkMode) {
                    super.onNetworkModeChanged(mode)
                }

                override fun onNetworkSpeedTestError(
                    errorCode: Int,
                    type: ZegoNetworkSpeedTestType
                ) {
                    super.onNetworkSpeedTestError(errorCode, type)
                    Log.i(TAG, "onNetworkSpeedTestError: $errorCode   type== $type")
                }

                override fun onNetworkSpeedTestQualityUpdate(
                    quality: ZegoNetworkSpeedTestQuality,
                    type: ZegoNetworkSpeedTestType
                ) {
                    super.onNetworkSpeedTestQualityUpdate(quality, type)
                    Log.i(
                        TAG,
                        "onNetworkSpeedTestQualityUpdate:  quality==  $quality   -and- type== $type   "
                    )
                }
            })

        engine.videoConfig = videoConfig

        /** Log in to the room  */
        engine!!.loginRoom(ROOM_NAME, user)
    }

    private var liveviewersCount = 0
    private var liveViewersListener = object : ChildEventListener {
        override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
            Log.i(TAG, "onChildAdded: Snapshot Id- ${snapshot.key}")

            var modelViewer: ModelViewer = snapshot.getValue(ModelViewer::class.java)!!
            if (modelViewer.isViewingLive) {
                listViewersPlayer.add(modelViewer)
                adapterViewersPlayer.notifyDataSetChanged()
            }
            liveviewersCount = listViewersPlayer.size
            tvLiveCount.text = liveviewersCount.toString()
        }

        override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
            var modelViewer: ModelViewer = snapshot.getValue(ModelViewer::class.java)!!
            if (!modelViewer.isViewingLive) {
                for (index in listViewersPlayer.indices) {
                    if (listViewersPlayer[index].userId.equals(modelViewer.userId)) {
                        listViewersPlayer.removeAt(index)
                        adapterViewersPlayer.notifyItemRemoved(index)
                        break
                    }
                }
            } else {
                listViewersPlayer.add(modelViewer)
                adapterViewersPlayer.notifyDataSetChanged()
            }
            liveviewersCount = listViewersPlayer.size
            tvLiveCount.text = liveviewersCount.toString()
        }

        override fun onChildRemoved(snapshot: DataSnapshot) {
            Log.i(TAG, "onChildRemoved: Snapshot Id- ${snapshot.key}")

            var modelViewer: ModelViewer = snapshot.getValue(ModelViewer::class.java)!!
            Log.i(TAG, "onChildRemoved: ${modelViewer.name}   ${modelViewer.userId}")

        }

        override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {

        }

        override fun onCancelled(error: DatabaseError) {

        }

    }

    private fun setViewersListener(streamID: String) {
        FirebaseListeners.viewersListener(streamID, liveViewersListener)
    }

    private fun removeViewersListener(streamId: String) {
        FirebaseListeners.removeViewerListener(streamId, liveViewersListener)
    }

    //setup viewers list
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var adapterViewersPlayer: AdapterViewersPlayer
    private fun setUpViewerRecycler() {
        linearLayoutManager =
            LinearLayoutManager(this@PublishActivity, LinearLayoutManager.HORIZONTAL, false)
        rvLiveViewers.layoutManager = linearLayoutManager
        adapterViewersPlayer = AdapterViewersPlayer(this@PublishActivity, listViewersPlayer, this)
        rvLiveViewers.apply {
            adapter = adapterViewersPlayer
        }
    }


    //set up messages recycler
    lateinit var linearLayoutManagerMessages: LinearLayoutManager
    lateinit var adapterMessages: AdapterMessages

    private fun setUpMessagesRecycler() {
        linearLayoutManagerMessages =
            LinearLayoutManager(this@PublishActivity, LinearLayoutManager.VERTICAL, false)
        rvMessages.layoutManager = linearLayoutManagerMessages
        adapterMessages = AdapterMessages(this@PublishActivity, listMessages, this)
        rvMessages.apply {
            adapter = adapterMessages
        }
    }

    override fun onStart() {
        super.onStart()

    }

    var liveStopped = false
    lateinit var customAlertDialog: CustomAlertDialog
    override fun onBackPressed() {
        customAlertDialog = CustomAlertDialog(
            getString(R.string.are_you_sure_want_to_stop),
            getString(R.string.yes),
            getString(R.string.no),
            this@PublishActivity, this
        )
        customAlertDialog.showDialog()

//        super.onBackPressed()
    }

    override fun onDestroy() {
        if (!liveStopped) {

            stopLiveStream()
        }
        super.onDestroy()
    }

    private fun stopStreaming() {
        engine.audioConfig = ZegoAudioConfig()
        engine.setAudioCaptureStereoMode(ZegoAudioCaptureStereoMode.NONE)
        engine.stopPreview()
        engine.setEventHandler(null)
        engine.stopPublishingStream()
        engine.logoutRoom(ROOM_NAME)
        ZegoExpressEngine.destroyEngine(null)
        videoConfig.codecID = ZegoVideoCodecID.DEFAULT
        liveStopped = true
        removeViewersListener(streamID)
        super.onBackPressed()
    }


    fun sendBroadcastMessage(message: String) {
        engine.sendBroadcastMessage(
            ROOM_NAME, message
        ) { errorCode, messageID ->

            if (errorCode == 0) {
                var zegoUser: ZegoUser = ZegoUser(
                    user.userID,
                    user.userName
                )
                val info: ModelBroadcastMessage = ModelBroadcastMessage(
                    message,
                    messageID,
                    System.currentTimeMillis(),
                    zegoUser
                )

                listMessages.add(info)
                setUpMessagesRecycler()
                rvMessages.scrollToPosition(listMessages.size - 1)
                etSendMessage.setText("")
                showToast("Message sent successfully")
            }
            Log.i(TAG, "sendBroadcastMessage: $messageID + errorCode- $errorCode")
        }
    }


    fun onSendMessageClicked(view: View) {
        var message = etSendMessage.text.toString()
        if (message.isNullOrEmpty()) {
            showToast("Message cannot be empty")
        } else {
            sendBroadcastMessage(message)
        }
    }

    override fun onMessageListItemClicked(position: Int, messageInfo: ModelBroadcastMessage) {

    }

    private var isFrontCam = true
    fun onCameraFlipClicked(view: View) {
        if (isFrontCam) {
            engine.useFrontCamera(false)
            isFrontCam = false
            Glide.with(this@PublishActivity).load(R.drawable.ic_baseline_camera_rear_24)
                .into(imgCameraFlip)
            showToast("Streaming Rear Cam")
        } else {
            engine.useFrontCamera(true)
            isFrontCam = true
            Glide.with(this@PublishActivity).load(R.drawable.ic_baseline_camera_front_24)
                .into(imgCameraFlip)
            showToast("Streaming Front Cam")
        }
        seekbarZoom.currentValue = 1.0f
    }

    fun cameraZoom() {
        seekbarZoom.setOnSeekBarChangeListener(object : CustomMinSeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Float, fromUser: Boolean) {
                if (engine != null) {
                    engine.setCameraZoomFactor(progress);
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?, progress: Float) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?, progress: Float) {

            }

        })
    }

    var isMicMuted = false
    fun onMuteMicClicked(view: View) {
        if (isMicMuted) {
            engine.muteMicrophone(false)
            Glide.with(this@PublishActivity).load(R.drawable.ic_baseline_mic_24)
                .into(imgMuteMic)
            isMicMuted = false
            showToast("Mic Unmuted")
        } else {
            engine.muteMicrophone(true)
            Glide.with(this@PublishActivity).load(R.drawable.ic_baseline_mic_off_24)
                .into(imgMuteMic)
            isMicMuted = true
            showToast("Mic muted")
        }
    }

    fun onStartLiveClicked(view: View) {
        if (roomConnectionState) {
            liveStartedBackendApi()    // hit api
        } else {
            showToast("Please try again")
        }
    }

    private fun liveStartedBackendApi() {
        Log.i(TAG, "liveStartedBackendApi: - $streamID    $ROOM_NAME")
        var publishLiveModel: PublishLiveModel = PublishLiveModel(
            streamId = streamID,
            roomName = ROOM_NAME,
            liveUserCoin = "0",
            liveUserCrown = "0",
            latitude = Constants.latitude.toString(),
            longitude = Constants.longitude.toString()
        )

        showLoading()
        goLiveAPi() // go live api our backend api
//        viewModel.publishUserLive(publishLiveModel)
    }

    private fun startStreamingZego() {
        tvMyName.text = user.userName
        engine!!.startPublishingStream(streamID)
        btnStartLive.visibility = View.GONE
        llSideBar.visibility = View.VISIBLE
        llBottomBar.visibility = View.VISIBLE
        rvLiveViewers.visibility = View.VISIBLE
        firebase()           // add user on firebase users list
        setViewersListener(streamID)
        startBGService()
    }

    private fun startBGService() {
        val intent = Intent(this@PublishActivity, LiveStreamService::class.java)
        intent.putExtra("liveId", liveId)
        startService(intent)
    }

    private val observer = androidx.lifecycle.Observer<PublishViewModel.ViewState> {
        hideLoading()
        if (it.isError) {
            it.error?.message?.toast()
        } else {
            it.publishData?.let { response ->
                if (API_TYPE == 1) {
                    var result = response as PublishLiveResponse
                    showToast("Live Started")
                    liveId = result.id
                    startStreamingZego()     //update with ZEGO lib and sgtart publishung the stream
                } else if (API_TYPE == 2) {
                    showToast("Live Ended")
                    stopStreaming()
                }
            }
        }
    }

    /* private val observerStopStream = androidx.lifecycle.Observer<PublishViewModel.ViewState> {
         hideLoading()
         if (it.isError) {
             it.error?.message?.toast()
         } else {
             it.publishData?.let { response ->
                 var result = response as PublishLiveResponse
                 showToast("Live Started")
                 stopStreaming()
                 //update with backend lib and stop publishung the stream
             }
         }
     }*/

    override fun onViewerListItemClicked(position: Int, user: ModelViewer) {

    }

    override fun onValuePosted(from: String) {
        Log.i(TAG, "onValuePosted: User Added On Firebase $from")
    }

    override fun onValueFailure(e: Exception) {
        Log.i(TAG, "onValueFailure: User Adding Failure with error-  ${e.message}")
    }

    override fun onNegativeButton() {
        Log.i(TAG, "onNegativeButton: dialog dismissed")
        customAlertDialog.dismiss()
    }

    override fun onPostiveButton() {
        if (liveId != null) {
            Log.i(TAG, "onPostiveButton: if live id not null $liveId")
            stopLiveStream()
//            showLoading()
//            viewModel.stopLiveStreaming(liveId!!)
        } else {
            Log.i(TAG, "onPostiveButton: else  live id null")
            stopStreaming()
        }
    }

    private fun goLiveAPi() {
        var publishLiveModel = PublishLiveModel(
            streamId = streamID,
            roomName = ROOM_NAME,
            liveUserCoin = "0",
            liveUserCrown = "0",
            latitude = Constants.latitude.toString(),
            longitude = Constants.longitude.toString()
        )

        viewModel.publishUserLive(publishLiveModel)
//        callGoLive = service!!.publishStreaming(publishLiveModel)
//        apiParse!!.hitApi(callGoLive, false, this@PublishActivity, this, isInternetConnected())
    }

    private fun stopLiveStream() {
        Log.i(TAG, "stopLiveStream: entered function")
//        callStopLive = service!!.stopLiveStreaming(liveId!!)
        API_TYPE=2
        showLoading()
        viewModel.stopLiveStreaming(liveId.toString())
//        apiParse!!.hitApi(callStopLive, false, this@PublishActivity, this, isInternetConnected())
    }

    override fun onSuccess(call: Call<JsonObject>, responseCode: Int, response: String) {
        if (call == callGoLive) {
            hideLoading()
            val liveResponse =
                Gson().fromJson(response, PublishResponse::class.java)
            liveId = liveResponse.result.id
            showToast(liveResponse.message!!)
            Log.i(TAG, "onSuccess: liveid---   $liveId")
            startStreamingZego() // start strwaming zego library
        } else if (call == callStopLive) {
            hideLoading()
            stopStreaming()   //go on back page
            showToast("live stopped")
            Log.i(TAG, "onSuccess: live stopped")
        }
    }

    override fun onError(call: Call<JsonObject>, errorCode: Int, errorMsg: String) {

    }

}