package com.app.talvido.liveStream.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.app.talvido.liveStream.models.ModelBroadcastMessage
import com.app.talvido.R
import kotlinx.android.synthetic.main.item_messages.view.*

class AdapterMessages(
    var context: Context,
    var listMessages: ArrayList<ModelBroadcastMessage>,
    var messageCallBack: MessageCallback
) : RecyclerView.Adapter<AdapterMessages.MyViewHolder>() {

    interface MessageCallback {
        fun onMessageListItemClicked(position: Int, messageInfo: ModelBroadcastMessage)
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(context).inflate(
            R.layout.item_messages, parent, false
        )
        return MyViewHolder(view)
    }

    private var lastAnimatedPosition = -1
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.tvMessage.text = listMessages[position].message
        holder.itemView.tvMessageSender.text = listMessages[position].fromUser.userName.toString()+" : "

        holder.itemView.setOnClickListener {
            messageCallBack.onMessageListItemClicked(position, listMessages[position])
        }
        if (position > lastAnimatedPosition) {
            lastAnimatedPosition = position
            val animation = AnimationUtils.loadAnimation(context, R.anim.down_to_up)
            animation.interpolator = AccelerateDecelerateInterpolator()
            holder.itemView.animation = animation
            animation.start()
        }


    }

    override fun getItemCount(): Int = listMessages.size
}