package com.app.talvido.liveStream.viewModel

import android.util.Log
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.app.talvido.base.presentation.viewmodel.BaseAction
import com.app.talvido.base.presentation.viewmodel.BaseViewModel
import com.app.talvido.base.presentation.viewmodel.BaseViewState
import com.app.talvido.business.data.network.ErrorModel
import com.app.talvido.business.data.network.Resource
import com.app.talvido.framwork.datasource.interactors.UserUseCase
import com.app.talvido.framwork.datasource.requestModel.AuthRequestModel
import com.app.talvido.framwork.datasource.requestModel.PublishLiveModel
import com.app.talvido.framwork.presentation.module.loginModule.LoginViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import retrofit2.http.Header


@ExperimentalCoroutinesApi
internal class PublishViewModel
@ViewModelInject
constructor(
    private val userUseCase: UserUseCase,
    @Assisted private val savedStateHandle: SavedStateHandle
) : BaseViewModel<PublishViewModel.ViewState, PublishViewModel.Action>(
    ViewState()
) {

    fun publishUserLive(publishLiveModel: PublishLiveModel) {
        viewModelScope.launch {
            userUseCase.publishLive(publishLiveModel).onEach { dataState ->
                when (dataState) {
                    is Resource.Success ->
                        sendAction(
                            Action.PublishLiveData(
                                data = dataState.data
                            )
                        )
                    is Resource.Error ->
                        sendAction(
                            Action.Error(
                                ErrorModel(dataState.msg!!, dataState.status!!)
                            )
                        )
                    is Resource.NetworkError -> sendAction(
                        Action.Error(
                            ErrorModel(dataState.msg!!, dataState.status!!)
                        )
                    )
                }
                Log.i("livepub", "publishUserLive: ${dataState}")
                Log.i("livepub", "publishUserLive: ${dataState?.data}")
            }.launchIn(viewModelScope)
        }
    }

    fun stopLiveStreaming(liveId: String) {
        viewModelScope.launch {
            userUseCase.stopLiveStream(liveId).onEach { dataState ->
                when (dataState) {
                    is Resource.Success ->
                        sendAction(
                            Action.PublishLiveData(
                                data = dataState.msg
                            )
                        )
                    is Resource.Error ->
                        sendAction(
                            Action.Error(
                                ErrorModel(dataState.msg!!, dataState.status!!)
                            )
                        )
                    is Resource.NetworkError -> sendAction(
                        Action.Error(
                            ErrorModel(dataState.msg!!, dataState.status!!)
                        )
                    )
                }
            }.launchIn(viewModelScope)
        }
    }

    internal data class ViewState(
        val isLoading: Boolean = true,
        val isError: Boolean = false,
        val error: ErrorModel? = null,
        val publishData: Any? = null

    ) : BaseViewState

    internal sealed class Action : BaseAction {
        class PublishLiveData(val data: Any?) : Action()
        class Error(val error: ErrorModel?) : Action()
    }

    override fun onReduceState(viewAction: PublishViewModel.Action) = when (viewAction) {
        is PublishViewModel.Action.Error -> state.copy(
            isLoading = false,
            isError = true,
            error = viewAction.error,
            publishData = null
        )

        is PublishViewModel.Action.PublishLiveData -> state.copy(
            isLoading = false,
            isError = false,
            error = null,
            publishData = viewAction.data
        )
    }
}