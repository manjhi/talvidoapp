package com.app.talvido.framwork.presentation.module.liveBottomModule

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.GpsStatus
import android.location.LocationManager
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.GridLayoutManager
import com.app.talvido.R
import com.app.talvido.base.presentation.fragment.BaseContainerFragment
import com.app.talvido.databinding.FragmentLiveBottomBinding
import com.app.talvido.liveStream.publisher.PublishActivity
import com.app.talvido.utils.Constants
import com.app.talvido.utils.onClick
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.android.synthetic.main.fragment_live_bottom.*


/**
 * A simple [Fragment] subclass.
 * Use the [LiveBottomFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class LiveBottomFragment : BaseContainerFragment<FragmentLiveBottomBinding>(),
    AdapterLiveUsers.LiveCallBack {
    private lateinit var binding: FragmentLiveBottomBinding
    private var TAG: String = "liveBottommFrag"
    override val layoutResourceId: Int
        get() = R.layout.fragment_live_bottom

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = getViewDataBinding()
        binding.liveShimmer.liveShimmerEffect.startShimmer()

        Handler().postDelayed({
            binding.liveShimmer.liveShimmerEffect.stopShimmer()
        }, 10000)

        binding.imgStartLive.onClick {
            checkPermission()
        }

        setUpRecycler()
        getLiveUsersData()
    }

    override fun onResume() {
        super.onResume()

    }

    private fun registerReceiver() {
        LocalBroadcastManager.getInstance(requireActivity()).registerReceiver(
            broadCastReceiver,
            IntentFilter("location_Broadcast")
        )
        Log.i(TAG, "registerReceiver: ")
    }

    private fun unRegisterReceiver() {
        LocalBroadcastManager.getInstance(requireActivity())
            .unregisterReceiver(broadCastReceiver) //remove broadcast listener ondestroy
        Log.i(TAG, "unRegisterReceiver: ")
    }

    private var broadCastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            try {
                unRegisterReceiver()
                val publishIntent: Intent =
                    Intent(requireActivity(), PublishActivity::class.java)
                startActivity(publishIntent)
                Log.i(TAG, "onReceive:latitude-- ${Constants.latitude}")
                Log.i(TAG, "onReceive:longitude-- ${Constants.longitude}")
            } catch (e: Exception) {
                Log.i(TAG, "onReceive: ${e.message}")
            }
        }
    }

    private fun getLiveUsersData() {

    }

    var listLive: ArrayList<String> = ArrayList()
    lateinit var adapterLiveUsers: AdapterLiveUsers
    lateinit var gridLayoutManager: GridLayoutManager
    private fun setUpRecycler() {
        gridLayoutManager = GridLayoutManager(context, 2)
        adapterLiveUsers = AdapterLiveUsers(requireActivity(), listLive, this)
        recyclerLiveUsers.apply {
            layoutManager = gridLayoutManager
            adapter = adapterLiveUsers
        }
    }

    private fun checkPermission() {
        Dexter.withActivity(activity)
            .withPermissions(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                        getLatLng()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<com.karumi.dexter.listener.PermissionRequest>?,
                    token: PermissionToken?
                ) {
                    showToast("Permissions Required")
                    token!!.continuePermissionRequest()
                }
            }).check()
    }

    private fun getLatLng() {
        if (isLocationEnabled()) {
            registerReceiver()
            getLastLocation()
        } else {
            showToast("Turn on GPS to start Streaming Live")
            openGPSSettings()
        }
    }

    override fun onLiveUserClicked() {

    }

    override fun onDetach() {
        unRegisterReceiver()
        super.onDetach()
    }

    override fun onDestroyView() {
        unRegisterReceiver()
        super.onDestroyView()
    }

    private fun isLocationEnabled(): Boolean {
        var locationManager =
            requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager

        var gpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        return gpsStatus
    }

    private fun openGPSSettings() {
        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
        startActivityForResult(intent, Constants.GPS_REQ_CODE)
    }
}