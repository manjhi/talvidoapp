package com.app.talvido.framwork.presentation.module.createVideoBottomModule

import ai.deepar.ar.ARErrorType
import ai.deepar.ar.AREventListener
import ai.deepar.ar.CameraResolutionPreset
import ai.deepar.ar.DeepAR
import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.hardware.Camera
import android.media.Image
import android.media.MediaScannerConnection
import android.os.Bundle
import android.os.Environment
import android.text.format.DateFormat
import android.util.DisplayMetrics
import android.view.Surface
import android.view.SurfaceHolder
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.app.talvido.R
import com.app.talvido.base.presentation.fragment.BaseContainerFragment
import com.app.talvido.databinding.FragmentCreateVideoBottomBinding
import com.app.talvido.utils.onClick
import com.app.talvido.utils.toast
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import timber.log.Timber
import java.io.File
import java.io.FileOutputStream
import java.util.*
import kotlin.collections.ArrayList

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class CreateVideoBottomFragment : BaseContainerFragment<FragmentCreateVideoBottomBinding>(),
    SurfaceHolder.Callback, AREventListener {
    override val layoutResourceId: Int
        get() = R.layout.fragment_create_video_bottom

    private lateinit var binding: FragmentCreateVideoBottomBinding
    private lateinit var deepAR: DeepAR
    private lateinit var cameraGrabber: CameraGrabber
    private val defaultCameraDevice = Camera.CameraInfo.CAMERA_FACING_FRONT
    private var cameraDevice = defaultCameraDevice
    private var screenOrientation = 0
    var masks: ArrayList<String> = ArrayList()
    var effects: ArrayList<String> = ArrayList()
    var filters: ArrayList<String> = ArrayList()
    private var recording = false
    private val recordingPath = Environment.getExternalStorageDirectory().toString() + File.separator + "video.mp4"
    private var activeFilterType = 0
    private var currentMask = 0
    private var currentEffect = 0
    private var currentFilter = 0
    private lateinit var videoPath: String

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = getViewDataBinding()
        deepAR = DeepAR(requireContext())
    }

//    override fun onStart() {
//        super.onStart()
//        setupCamera()
//    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1 && grantResults.size > 0) {
            for (grantResult in grantResults) {
                if (grantResult != PackageManager.PERMISSION_GRANTED) {
                    return  // no permission
                }
                initialize()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (ContextCompat.checkSelfPermission(
                requireContext(), Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                requireContext(), Manifest.permission.RECORD_AUDIO
            ) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                requireContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.RECORD_AUDIO
                ),
                1
            )
        } else {
            // Permission has already been granted
            initialize()
        }
    }

    private fun initialize() {
        initializeDeepAR()
        initializeFilters()
        setSurface()
        setClicks()
    }

    private fun initializeFilters() {
        masks = ArrayList<String>()
        masks.add("none")
        masks.add("aviators")
        masks.add("bigmouth")
        masks.add("dalmatian")
        masks.add("flowers")
        masks.add("koala")
        masks.add("lion")
        masks.add("smallface")
        masks.add("teddycigar")
        masks.add("background_segmentation")
        masks.add("tripleface")
        masks.add("sleepingmask")
        masks.add("fatify")
        masks.add("obama")
        masks.add("mudmask")
        masks.add("pug")
        masks.add("slash")
        masks.add("twistedface")
        masks.add("grumpycat")
        effects = ArrayList<String>()
        effects.add("none")
        effects.add("fire")
        effects.add("rain")
        effects.add("heart")
        effects.add("blizzard")
        filters = ArrayList<String>()
        filters.add("none")
        filters.add("filmcolorperfection")
        filters.add("tv80")
        filters.add("drawingmanga")
        filters.add("sepia")
        filters.add("bleachbypass")
    }

    private fun setClicks() {
        binding.switchCamera.onClick {
            cameraDevice =
                if (cameraGrabber.currCameraDevice === Camera.CameraInfo.CAMERA_FACING_FRONT) Camera.CameraInfo.CAMERA_FACING_BACK else Camera.CameraInfo.CAMERA_FACING_FRONT
            cameraGrabber.changeCameraDevice(cameraDevice)
        }

        binding.recordButton.onClick {
            if (recording) {
                binding.innerRecord.setBackgroundResource(R.drawable.yellow_circle)
                deepAR.stopVideoRecording()
                "Saved video to: $videoPath".toast()
                var bundle = Bundle()
                bundle.putString("videoPath", videoPath)
                moveNextArgs(R.id.action_upload_to_playVideoFragment, bundle)

            } else {
                binding.innerRecord.setBackgroundResource(android.R.color.transparent)
                val fileName = "DEEPVID" + System.currentTimeMillis() + ".mp4"
                deepAR.startVideoRecording(createVideoPath(requireContext(), fileName))
                "Started video recording!".toast()
            }
            recording = !recording
        }

        binding.mask.onClick {
            binding.mask.setBackgroundColor(resources.getColor(R.color.darkyellow))
            activeFilterType = 0
        }
        binding.effect.onClick {
            if (activeFilterType == 1) {
                activeFilterType = 0
            } else {
                activeFilterType = 1
            }
        }
        binding.filter.onClick {
            if (activeFilterType == 2) {
                binding.filter.setTextColor(resources.getColor(R.color.white))
                activeFilterType = 0
            } else {
                binding.filter.setTextColor(resources.getColor(R.color.darkyellow))
                activeFilterType = 2

            }
        }

        binding.relativeLayout.setOnTouchListener(object : OnSwipeTouchListener(requireActivity()) {
            override fun onSwipeLeft() {
                Timber.e("Swipe Left")
                gotoNext()
            }

            override fun onSwipeRight() {
                gotoPrevious()
                Timber.e("Swipe Right")
            }
        })
    }

    fun createVideoPath(
        context: Context,
        fileName: String?
    ): String? {
        val imageThumbsDirectory = context.getExternalFilesDir("FOLDER")
        if (imageThumbsDirectory != null) {
            if (!imageThumbsDirectory.exists()) {
                Toast.makeText(context, "Created", Toast.LENGTH_SHORT).show()
                imageThumbsDirectory.mkdir()
            }
        }
        val appDir = context.getExternalFilesDir("FOLDER")!!.absolutePath
        val file = File(appDir, fileName)
        Toast.makeText(context, file.absolutePath, Toast.LENGTH_SHORT).show()
        videoPath = file.absolutePath
        return file.absolutePath
    }

    private fun getScreenOrientation(): Int {
        val rotation: Int = requireActivity().getWindowManager().getDefaultDisplay().getRotation()
        val dm = DisplayMetrics()
        requireActivity().getWindowManager().getDefaultDisplay().getMetrics(dm)
        val width = dm.widthPixels
        val height = dm.heightPixels
        val orientation: Int
        // if the device's natural orientation is portrait:
        orientation = if ((rotation == Surface.ROTATION_0
                    || rotation == Surface.ROTATION_180) && height > width ||
            (rotation == Surface.ROTATION_90
                    || rotation == Surface.ROTATION_270) && width > height
        ) {
            when (rotation) {
                Surface.ROTATION_0 -> ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                Surface.ROTATION_90 -> ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                Surface.ROTATION_180 -> ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT
                Surface.ROTATION_270 -> ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE
                else -> ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            }
        } else {
            when (rotation) {
                Surface.ROTATION_0 -> ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                Surface.ROTATION_90 -> ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                Surface.ROTATION_180 -> ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE
                Surface.ROTATION_270 -> ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT
                else -> ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
            }
        }
        return orientation
    }

    private fun initializeDeepAR() {
        deepAR = DeepAR(requireContext())
        deepAR.setLicenseKey("53303a0d214f61312fdd378099c367891ec9b59a68dc3f1a7d8c418864f9c8d0d249467fe3fdea77")
        deepAR.initialize(requireContext(), this)
        setupCamera()
    }

    private fun setupCamera() {
        cameraGrabber = CameraGrabber(cameraDevice)
        screenOrientation = getScreenOrientation()
        when (screenOrientation) {
            ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE -> cameraGrabber.setScreenOrientation(90)
            ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE -> cameraGrabber.setScreenOrientation(
                270
            )
            else -> cameraGrabber.setScreenOrientation(0)
        }

        // Available 1080p, 720p and 480p resolutions
        cameraGrabber.setResolutionPreset(CameraResolutionPreset.P1280x720)
        val context: Activity = requireActivity()
        cameraGrabber.initCamera(object : CameraGrabberListener {
            override fun onCameraInitialized() {
                cameraGrabber.setFrameReceiver(deepAR)
                cameraGrabber.startPreview()
            }

            override fun onCameraError(errorMsg: String?) {
                val builder =
                    AlertDialog.Builder(context)
                builder.setTitle("Camera error")
                builder.setMessage(errorMsg)
                builder.setCancelable(true)
                builder.setPositiveButton(
                    "Ok"
                ) { dialogInterface, i -> dialogInterface.cancel() }
                val dialog = builder.create()
                dialog.show()
            }
        })
    }

    private fun getFilterPath(filterName: String): String? {
        return if (filterName == "none") {
            null
        } else "file:///android_asset/" + filterName
    }

    private fun gotoNext() {
        if (activeFilterType == 0) {
            currentMask = (currentMask + 1) % masks.size
            deepAR.switchEffect("mask", getFilterPath(masks[currentMask]))
        } else if (activeFilterType == 1) {
            currentEffect = (currentEffect + 1) % effects.size
            deepAR.switchEffect("effect", getFilterPath(effects[currentEffect]))
        } else if (activeFilterType == 2) {
            currentFilter = (currentFilter + 1) % filters.size
            deepAR.switchEffect("filter", getFilterPath(filters[currentFilter]))
        }
    }

    private fun gotoPrevious() {
        if (activeFilterType == 0) {
            currentMask = (currentMask - 1) % masks.size
            if (currentMask < 0) {
                currentMask = masks.size - 1
            }
            deepAR.switchEffect("mask", getFilterPath(masks[currentMask]))
        } else if (activeFilterType == 1) {
            currentEffect = (currentEffect - 1) % effects.size
            deepAR.switchEffect("effect", getFilterPath(effects[currentEffect]))
        } else if (activeFilterType == 2) {
            currentFilter = (currentFilter - 1) % filters.size
            deepAR.switchEffect("filter", getFilterPath(filters[currentFilter]))
        }
    }


    override fun onStop() {
        super.onStop()
        if (cameraGrabber == null) {
            return
        }
        cameraGrabber.setFrameReceiver(null)
        cameraGrabber.stopPreview()
        cameraGrabber.releaseCamera()
//        cameraGrabber = null
    }

    override fun onDestroy() {
        super.onDestroy()
        if (deepAR == null) {
            return
        }
        deepAR.setAREventListener(null)
        deepAR.release()
//        deepAR = null
    }


    private fun setSurface() {
        binding.surface.onClick {
            deepAR.onClick()
        }
        binding.surface.getHolder().addCallback(this)
        binding.surface.visibility = View.GONE
        binding.surface.visibility = View.VISIBLE
    }

    override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {

        // If we are using on screen rendering we have to set surface view where DeepAR will render
        deepAR.setRenderSurface(holder.surface, width, height)
    }

    override fun surfaceDestroyed(holder: SurfaceHolder) {
        if (deepAR != null) {
            deepAR.setRenderSurface(null, 0, 0)
        }
    }

    override fun surfaceCreated(holder: SurfaceHolder) {
    }

    override fun shutdownFinished() {

    }

    override fun faceVisibilityChanged(p0: Boolean) {

    }

    override fun videoRecordingFailed() {

    }

    override fun videoRecordingPrepared() {

    }

    override fun initialized() {

    }

    override fun error(p0: ARErrorType?, p1: String?) {

    }

    override fun screenshotTaken(p0: Bitmap?) {
        val now =
            DateFormat.format("yyyy_MM_dd_hh_mm_ss", Date())
        try {
            val imageFile = File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                    .toString() + "/DeepAR_" + now + ".jpg"
            )
            val outputStream = FileOutputStream(imageFile)
            val quality = 100
            p0?.compress(Bitmap.CompressFormat.JPEG, quality, outputStream)
            outputStream.flush()
            outputStream.close()
            MediaScannerConnection.scanFile(
                requireContext(),
                arrayOf(imageFile.toString()),
                null,
                null
            )
            Toast.makeText(requireContext(), "Screenshot saved", Toast.LENGTH_SHORT).show()
        } catch (e: Throwable) {
            e.printStackTrace()
        }

    }

    override fun effectSwitched(p0: String?) {

    }

    override fun videoRecordingStarted() {

    }

    override fun frameAvailable(p0: Image?) {

    }

    override fun videoRecordingFinished() {

    }

    override fun imageVisibilityChanged(p0: String?, p1: Boolean) {

    }


}