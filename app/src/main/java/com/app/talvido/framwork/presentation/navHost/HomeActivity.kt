package com.app.talvido.framwork.presentation.navHost

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.app.talvido.R
import com.app.talvido.business.localData.PrefsManager
import com.app.talvido.framwork.datasource.model.AuthResponse
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import com.app.talvido.utils.Gone
import com.app.talvido.utils.Visible
import com.app.talvido.utils.loadImage
import com.app.talvido.utils.onClick
import dagger.hilt.android.AndroidEntryPoint
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import timber.log.Timber


@ExperimentalCoroutinesApi
@AndroidEntryPoint
class HomeActivity : AppCompatActivity() {

    private lateinit var drawer: DrawerLayout
    private lateinit var navigation: NavigationView
    private lateinit var iv_menu: CircleImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        var bottom: BottomNavigationView = findViewById(R.id.bottom_navigation)

        bottom.itemIconTintList = null

        navigation = findViewById(R.id.nav_view)
        navigation.itemIconTintList = null

        var navController = findNavController(R.id.fragNavHost)

        navigation.setupWithNavController(navController)
        bottom.setupWithNavController(navController)


        iv_menu = findViewById(R.id.iv_menu)
        drawer = findViewById(R.id.drawerLayout)
        iv_menu.onClick {
            drawer.openDrawer(GravityCompat.START)
        }

        navController.addOnDestinationChangedListener { controller, destination, arguments ->
            if (destination.id == R.id.home || destination.id == R.id.profile) {
                bottom.Visible()
                topBar.Visible()
            } else {
                bottom.Gone()
                topBar.Gone()
            }
        }

        navigation.menu.findItem(R.id.logout).setOnMenuItemClickListener {
            Timber.e("Logout")
            drawer.closeDrawer(GravityCompat.START)
            LogOutAlert()
            true
        }
    }

    private fun LogOutAlert() {
        val builder = AlertDialog.Builder(this)
        //set title for alert dialog
        builder.setTitle("Logout")
        //set message for alert dialog
        builder.setMessage("Are you sure?")
        //performing positive action
        builder.setPositiveButton("Yes") { dialogInterface, which ->
            dialogInterface.dismiss()
            PrefsManager.get().removeAll()
            startActivity(Intent(this, MainActivity::class.java))
            finishAffinity()
        }
        //performing negative action
        builder.setNegativeButton("No") { dialogInterface, which ->
            dialogInterface.dismiss()
        }
        // Create the AlertDialog
        val alertDialog: AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    override fun onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onResume() {
        super.onResume()
        val user by lazy {
            PrefsManager.get().getObject(PrefsManager.PREF_USER_PROFILE, AuthResponse::class.java)
        }

        var hView = navigation.getHeaderView(0)
        var tv_userId = hView.findViewById<TextView>(R.id.tv_userId)
        var tv_usernameHeader = hView.findViewById<TextView>(R.id.tv_usernameHeader)
        var tv_followingHeader = hView.findViewById<TextView>(R.id.tv_followingHeader)
        var cv_imageHeader = hView.findViewById<CircleImageView>(R.id.cv_imageHeader)
        tv_userId.text = user?.username
        tv_usernameHeader.text = user?.name
        cv_imageHeader.loadImage(user?.image)
        iv_menu.loadImage(user?.image)
    }
}