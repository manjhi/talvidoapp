package com.app.talvido.framwork.presentation.module.homeModule

import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.View
import com.app.talvido.R
import com.app.talvido.base.presentation.fragment.BaseContainerFragment
import com.app.talvido.databinding.FragmentHomeBinding
import com.app.talvido.framwork.presentation.module.homeModule.adapters.CountryAdapter
import com.app.talvido.framwork.presentation.module.homeModule.adapters.VideosAdapter
import com.app.talvido.utils.ScreenSlidePagerAdapter
import com.google.android.material.tabs.TabLayoutMediator
import com.app.talvido.utils.Gone
import com.app.talvido.utils.Visible
import com.app.talvido.utils.setManager
import java.util.*
import kotlin.collections.ArrayList

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : BaseContainerFragment<FragmentHomeBinding>() {

    private lateinit var binding: FragmentHomeBinding

    override val layoutResourceId: Int
        get() = R.layout.fragment_home


    private var currentPage=0


    private lateinit var countyAdapter: CountryAdapter
    private lateinit var videoAdapter: VideosAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = getViewDataBinding()

       binding.homeShimmer.shimmerEffect.startShimmer()

        Handler().postDelayed({
            binding.homeShimmer.shimmerEffect.stopShimmer()
            binding.homeShimmer.shimmerEffect.Gone()
            binding.viewContainer.Visible()
        },5000)


        val pagerAdapter =
            ScreenSlidePagerAdapter(this,getListOfPagerContents(),3)
        binding.bannerPager.adapter=pagerAdapter

        TabLayoutMediator(binding.intoTabLayout,binding.bannerPager){tab, position ->

        }.attach()

        val handler = Handler()
        val update = Runnable {
            if (currentPage == getListOfPagerContents().size) {
                currentPage = 0
            }

            //The second parameter ensures smooth scrolling
            binding.bannerPager.setCurrentItem(currentPage++, true)
        }

        Timer().schedule(object : TimerTask() {
            // task to be scheduled
            override fun run() {
                handler.post(update)
            }
        }, 3500, 3500)


        setAdapter()
    }

    private fun setAdapter() {
        binding.countyRecyclerView.setManager(isItHorizontal = true)
        countyAdapter= CountryAdapter()
        binding.countyRecyclerView.adapter=countyAdapter
        var list:ArrayList<String> = ArrayList()
        list.add("1")
        list.add("2")
        list.add("3")
        countyAdapter.submitList(list)

        videoAdapter= VideosAdapter()
        binding.recyclerView.setManager(isItGrid = true,spanCount = 2)
        binding.recyclerView.adapter=videoAdapter
        var vList:ArrayList<String> = ArrayList()
        vList.add("1")
        vList.add("2")
        vList.add("3")
        vList.add("4")
        vList.add("5")
        vList.add("6")
        vList.add("7")
        vList.add("8")
        vList.add("9")
        vList.add("10")
        vList.add("11")
        vList.add("12")
        vList.add("13")
        vList.add("14")
        vList.add("15")
        vList.add("16")
        videoAdapter.submitList(vList)
    }

    fun getListOfPagerContents(): List<Array<String>> {
        val ar1 = arrayOf("Hello Talvido", "Live Streaming","R" )
        val ar2 = arrayOf("Hello Talvido", "Videos","G")
        val ar3 = arrayOf("Hello Talvido", "Live","B")
        return listOf(ar1,ar2,ar3)
    }
}