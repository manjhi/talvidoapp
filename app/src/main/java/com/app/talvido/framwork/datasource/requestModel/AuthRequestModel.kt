package com.app.talvido.framwork.datasource.requestModel

import java.io.Serializable

class AuthRequestModel(
    var phone: String? = null,
    var deviceId: String? = null,
    var password: String? = null,
    var name: String? = null,
    var country:String?=null,
    var reg_id: String? = null,
    var device_type: String = "android"
):Serializable