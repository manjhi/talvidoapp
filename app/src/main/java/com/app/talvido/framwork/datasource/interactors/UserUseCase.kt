package com.app.talvido.framwork.datasource.interactors


import com.google.gson.JsonObject
import com.app.talvido.business.data.network.Resource
import com.app.talvido.business.data.util.apiRequest
import com.app.talvido.framwork.datasource.model.AuthResponse
import com.app.talvido.framwork.datasource.requestModel.AuthRequestModel
import com.app.talvido.framwork.datasource.requestModel.PublishLiveModel
import com.app.talvido.framwork.datasource.retrofit.AppApiServices
import com.app.talvido.liveStream.models.PublishLiveResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class UserUseCase
constructor(
    private val networkDataSource: AppApiServices
) {
    suspend fun login(authRequestModel: AuthRequestModel): Flow<Resource<AuthResponse>?> = flow {
        val data = apiRequest(Dispatchers.IO) {
            networkDataSource.login(authRequestModel)
        }
        emit(data)
    }


    suspend fun validatePhone(authRequestModel: AuthRequestModel): Flow<Resource<Any>?> = flow {
        val data = apiRequest(Dispatchers.IO) {
            networkDataSource.validatePhone(authRequestModel)
        }
        emit(data)
    }

    suspend fun registerUser(authRequestModel: AuthRequestModel): Flow<Resource<AuthResponse>?> =
        flow {
            val data = apiRequest(Dispatchers.IO) {
                networkDataSource.registerUser(authRequestModel)
            }
            emit(data)
        }

    suspend fun publishLive(
        publishLiveModel: PublishLiveModel
    ): Flow<Resource<PublishLiveResponse>?> = flow {
        val data = apiRequest(Dispatchers.IO) {
            networkDataSource.publishLive(publishLiveModel)
        }
        emit(data)
    }


    suspend fun stopLiveStream(
        liveId: String
    ): Flow<Resource<Any>?> = flow {
        val data = apiRequest(Dispatchers.IO) {
            networkDataSource.stopLiveStream(liveId)
        }
        emit(data)
    }
}