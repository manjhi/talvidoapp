package com.app.talvido.framwork.presentation.module.profileBottomModule

import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.talvido.R
import com.app.talvido.base.presentation.fragment.BaseContainerFragment
import com.app.talvido.databinding.FragmentProfileBottomBinding
import com.app.talvido.framwork.presentation.module.homeModule.adapters.VideosAdapter
import com.app.talvido.framwork.presentation.module.profileBottomModule.adapters.ProfileVideoAdapter
import com.app.talvido.utils.Gone
import com.app.talvido.utils.ScreenSlidePagerAdapter
import com.app.talvido.utils.loadImage
import com.app.talvido.utils.setManager


/**
 * A simple [Fragment] subclass.
 * Use the [ProfileBottomFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ProfileBottomFragment : BaseContainerFragment<FragmentProfileBottomBinding>() {


    private lateinit var binding: FragmentProfileBottomBinding

    override val layoutResourceId: Int
        get() = R.layout.fragment_profile_bottom


    private lateinit var videoAdapter: ProfileVideoAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = getViewDataBinding()
        binding.profileShimmer.profileShimmerEffect.startShimmer()

        Handler().postDelayed({
            binding.profileShimmer.profileShimmerEffect.stopShimmer()
        }, 10000)


        binding.imgUserProfileCrown.Gone()
        setAdapter()
    }

    private fun setAdapter() {
        binding.recyclerVideosProfile.setManager(isItGrid = true, spanCount = 3)
        videoAdapter= ProfileVideoAdapter()
        binding.recyclerVideosProfile.adapter = videoAdapter

        var list: ArrayList<String> = ArrayList()
        list.add("1")
        list.add("2")
        list.add("3")
        list.add("4")
        list.add("5")
        list.add("6")
        list.add("7")
        list.add("8")
        list.add("9")
        list.add("10")
        list.add("11")
        videoAdapter.submitList(list)
    }
}