package com.app.talvido.framwork.presentation.module.walletModule

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.talvido.R
import com.app.talvido.base.presentation.fragment.BaseContainerFragment
import com.app.talvido.databinding.FragmentWalletBinding
import com.app.talvido.framwork.presentation.module.walletModule.adapters.WalletFragmentPagerAdapter
import com.app.talvido.utils.onClick
import com.google.android.material.tabs.TabLayoutMediator

/**
 * A simple [Fragment] subclass.
 * Use the [WalletFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class WalletFragment : BaseContainerFragment<FragmentWalletBinding>() {

    private lateinit var binding: FragmentWalletBinding

    override val layoutResourceId: Int
        get() = R.layout.fragment_wallet


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = getViewDataBinding()

        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("My Diamonds").setIcon(R.drawable.diamondnew))
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("My Beans").setIcon(R.drawable.beannew))
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Income").setIcon(R.drawable.ic_dollar))
        binding.tabLayout.getTabAt(0)?.icon?.colorFilter = null
        binding.tabLayout.getTabAt(1)?.icon?.colorFilter = null
        binding.tabLayout.getTabAt(2)?.icon?.colorFilter = null

        var tabAdpater=WalletFragmentPagerAdapter(this,3)
        binding.viewPager.adapter=tabAdpater

        TabLayoutMediator(binding.tabLayout,binding.viewPager){tab, position ->
            when(position){
                0->{
                   tab.text="My Diamonds"
                    tab.setIcon(R.drawable.diamondnew)
                }
                1->{
                    tab.text="My Beans"
                    tab.setIcon(R.drawable.beannew)

                }
                2->{
                    tab.text="Income"
                    tab.setIcon(R.drawable.ic_dollar)

                }
            }
        }.attach()

        binding.backArrow.onClick {
            moveBack()
        }
    }
}