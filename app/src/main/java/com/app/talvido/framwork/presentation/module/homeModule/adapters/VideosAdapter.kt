package com.app.talvido.framwork.presentation.module.homeModule.adapters

import com.app.talvido.R
import com.app.talvido.base.presentation.adapter.DataBindingAdapter
import com.app.talvido.base.presentation.adapter.DataBindingViewHolder
import com.app.talvido.base.presentation.adapter.DiffCallback
import com.app.talvido.utils.loadImage
import kotlinx.android.synthetic.main.item_home_video_list.view.*

class VideosAdapter :DataBindingAdapter<String>(DiffCallback()){
    override fun getItemViewType(position: Int): Int {
        return R.layout.item_home_video_list
    }

    override fun onBindViewHolder(holder: DataBindingViewHolder<String>, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.itemView.let {
            it.videoThumbnil.loadImage("https://picsum.photos/200/300")
        }
    }
}