package com.app.talvido.framwork.datasource.retrofit


import com.google.gson.JsonObject
import com.app.talvido.framwork.datasource.model.ApiResponse
import com.app.talvido.framwork.datasource.model.AuthResponse
import com.app.talvido.framwork.datasource.requestModel.AuthRequestModel
import com.app.talvido.framwork.datasource.requestModel.PublishLiveModel
import com.app.talvido.liveStream.models.PublishLiveResponse
import retrofit2.Response


interface AppApiServices {
    suspend fun login(authRequestModel: AuthRequestModel): Response<ApiResponse<AuthResponse>>

    suspend fun validatePhone(authRequestModel: AuthRequestModel): Response<ApiResponse<Any>>

    suspend fun registerUser(authRequestModel: AuthRequestModel): Response<ApiResponse<AuthResponse>>

    suspend fun publishLive(publishLiveModel: PublishLiveModel): Response<ApiResponse<PublishLiveResponse>>

    suspend fun stopLiveStream(liveId:String): Response<ApiResponse<Any>>
}