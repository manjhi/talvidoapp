package com.app.talvido.framwork.presentation.module.walletModule.adapters

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.app.talvido.framwork.presentation.module.homeModule.ViewPagerContentFragment
import com.app.talvido.framwork.presentation.module.walletModule.IncomeFragment
import com.app.talvido.framwork.presentation.module.walletModule.MyBeansFragment
import com.app.talvido.framwork.presentation.module.walletModule.MyDiamondFragment

class WalletFragmentPagerAdapter(
    fragment: Fragment,
    val mPageNumbers: Int
) : FragmentStateAdapter(fragment) {

    override fun getItemCount(): Int = mPageNumbers

    override fun createFragment(position: Int): Fragment {
        when (position) {
            0 -> {
                return MyDiamondFragment()
            }
            1 -> {
                return MyBeansFragment()
            }
            2 -> {
                return IncomeFragment()
            }
            else -> {
                return MyDiamondFragment()
            }
        }
    }
}