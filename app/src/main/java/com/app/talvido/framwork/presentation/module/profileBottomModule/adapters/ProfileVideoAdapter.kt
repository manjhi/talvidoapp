package com.app.talvido.framwork.presentation.module.profileBottomModule.adapters

import com.app.talvido.R
import com.app.talvido.base.presentation.adapter.DataBindingAdapter
import com.app.talvido.base.presentation.adapter.DataBindingViewHolder
import com.app.talvido.base.presentation.adapter.DiffCallback
import com.app.talvido.utils.loadImage
import kotlinx.android.synthetic.main.list_videos_profile.view.*

class ProfileVideoAdapter : DataBindingAdapter<String>(DiffCallback()) {

    override fun getItemViewType(position: Int): Int {
        return R.layout.list_videos_profile
    }

    override fun onBindViewHolder(holder: DataBindingViewHolder<String>, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.itemView.let {
            it.img_profile_videos.loadImage("https://picsum.photos/300/300")
        }
    }
}