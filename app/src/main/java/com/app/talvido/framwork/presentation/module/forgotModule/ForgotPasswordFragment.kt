package com.app.talvido.framwork.presentation.module.forgotModule

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import com.app.talvido.R
import com.app.talvido.base.presentation.fragment.BaseContainerFragment
import com.app.talvido.databinding.FragmentForgotPasswordBinding
import com.app.talvido.utils.onClick




/**
 * A simple [Fragment] subclass.
 * Use the [ForgotPasswordFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ForgotPasswordFragment : BaseContainerFragment<FragmentForgotPasswordBinding>() {

    private lateinit var binding: FragmentForgotPasswordBinding

    override val layoutResourceId: Int
        get() = R.layout.fragment_forgot_password


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = getViewDataBinding()

        setClicks()
    }

    private fun setClicks() {
        binding.backButton.onClick {
            moveBack()
        }

        binding.sendotpButton.onClick {
          //  moveNext(R.id.action_forgotPasswordFragment_to_verificationFragment)
        }
    }

}