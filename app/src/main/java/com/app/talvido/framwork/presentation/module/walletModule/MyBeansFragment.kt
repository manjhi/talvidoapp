package com.app.talvido.framwork.presentation.module.walletModule

import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.talvido.R
import com.app.talvido.base.presentation.fragment.BaseContainerFragment
import com.app.talvido.databinding.FragmentMyBeansBinding


/**
 * A simple [Fragment] subclass.
 * Use the [MyBeansFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MyBeansFragment : BaseContainerFragment<FragmentMyBeansBinding>() {

    private lateinit var binding: FragmentMyBeansBinding

    override val layoutResourceId: Int
        get() = R.layout.fragment_my_beans


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = getViewDataBinding()

        binding.swipeRefresh.setOnRefreshListener {
            Handler().postDelayed({
                binding.swipeRefresh.isRefreshing = false
            }, 2000)
        }
    }
}