package com.app.talvido.framwork.presentation.navHost

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.app.talvido.R
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import timber.log.Timber


@ExperimentalCoroutinesApi
@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Timber.e("DATA")

        /*var navHostFragment=supportFragmentManager.findFragmentById(R.id.fragment) as NavHostFragment
        val inflater=navHostFragment.navController.navInflater
        val graph=inflater.inflate(R.navigation.authentication)
        graph.startDestination=R.id.splashFragment
        navHostFragment.navController.graph=graph*/
    }
}