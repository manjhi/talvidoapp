package com.app.talvido.framwork.presentation.module.homeModule.adapters

import com.app.talvido.R
import com.app.talvido.base.presentation.adapter.DataBindingAdapter
import com.app.talvido.base.presentation.adapter.DiffCallback

class CountryAdapter : DataBindingAdapter<String>(DiffCallback()) {

    override fun getItemViewType(position: Int): Int {
        return R.layout.item_county_flag_layout
    }
}