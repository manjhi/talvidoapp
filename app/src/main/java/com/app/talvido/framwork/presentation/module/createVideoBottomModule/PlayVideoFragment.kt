package com.app.talvido.framwork.presentation.module.createVideoBottomModule

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.talvido.R
import com.app.talvido.base.presentation.fragment.BaseContainerFragment
import com.app.talvido.databinding.FragmentCreateVideoBottomBinding
import com.app.talvido.databinding.FragmentPlayVideoBinding


class PlayVideoFragment : BaseContainerFragment<FragmentPlayVideoBinding>() {

    override val layoutResourceId: Int
        get() = R.layout.fragment_play_video
    private lateinit var binding: FragmentPlayVideoBinding


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding= getViewDataBinding()
        var path = arguments?.getString("videoPath")
        if (path != null) {
            play(path)
        }


    }

    private fun play( path : String) {
        binding.videoView.setVideoPath(path)
        binding.videoView.start()
    }


}