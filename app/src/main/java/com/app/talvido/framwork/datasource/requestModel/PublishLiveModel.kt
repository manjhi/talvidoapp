package com.app.talvido.framwork.datasource.requestModel

import java.io.Serializable

class PublishLiveModel(
    var streamId: String?=null,
    var roomName: String?=null,
    var liveUserCoin: String?=null,
    var liveUserCrown: String?=null,
    var latitude:String?=null,
    var longitude:String?=null
)