package com.app.talvido.framwork.presentation.module.loginModule

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonObject
import com.app.talvido.base.presentation.viewmodel.BaseAction
import com.app.talvido.base.presentation.viewmodel.BaseViewModel
import com.app.talvido.base.presentation.viewmodel.BaseViewState
import com.app.talvido.business.data.network.ErrorModel
import com.app.talvido.business.data.network.Resource
import com.app.talvido.framwork.datasource.interactors.UserUseCase
import com.app.talvido.framwork.datasource.requestModel.AuthRequestModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch


@ExperimentalCoroutinesApi
internal class LoginViewModel
@ViewModelInject
constructor(
    private val userUseCase: UserUseCase,
    @Assisted private val savedStateHandle: SavedStateHandle
) : BaseViewModel<LoginViewModel.ViewState, LoginViewModel.Action>(
    ViewState()
) {


    fun login(authRequestModel: AuthRequestModel) {
        viewModelScope.launch {
            userUseCase.login(authRequestModel).onEach { dataState ->
                when (dataState) {
                    is Resource.Success ->
                        sendAction(
                            Action.LoginApiData(
                                data = dataState.data
                            )
                        )
                    is Resource.Error ->
                        sendAction(
                            Action.Error(
                                ErrorModel(dataState.msg!!, dataState.status!!)
                            )
                        )
                    is Resource.NetworkError -> sendAction(
                        Action.Error(
                            ErrorModel(dataState.msg!!, dataState.status!!)
                        )
                    )
                }
            }.launchIn(viewModelScope)
        }
    }


    fun validatePhone(authRequestModel: AuthRequestModel) {
        viewModelScope.launch {
            userUseCase.validatePhone(authRequestModel).onEach { dataState ->
                when (dataState) {
                    is Resource.Success ->
                        sendAction(
                            Action.LoginApiData(
                                data = dataState.data
                            )
                        )
                    is Resource.Error ->
                        sendAction(
                            Action.Error(
                                ErrorModel(dataState.msg!!, dataState.status!!)
                            )
                        )
                    is Resource.NetworkError -> sendAction(
                        Action.Error(
                            ErrorModel(dataState.msg!!, dataState.status!!)
                        )
                    )
                }
            }.launchIn(viewModelScope)
        }
    }

    fun registerUser(authRequestModel: AuthRequestModel) {
        viewModelScope.launch {
            userUseCase.registerUser(authRequestModel).onEach { dataState ->
                when (dataState) {
                    is Resource.Success ->
                        sendAction(
                            Action.LoginApiData(
                                data = dataState.data
                            )
                        )
                    is Resource.Error ->
                        sendAction(
                            Action.Error(
                                ErrorModel(dataState.msg!!, dataState.status!!)
                            )
                        )
                    is Resource.NetworkError -> sendAction(
                        Action.Error(
                            ErrorModel(dataState.msg!!, dataState.status!!)
                        )
                    )
                }
            }.launchIn(viewModelScope)
        }
    }


    internal data class ViewState(
        val isLoading: Boolean = true,
        val isError: Boolean = false,
        val error: ErrorModel? = null,
        val loginData: Any? = null

    ) : BaseViewState

    internal sealed class Action : BaseAction {
        class LoginApiData(val data: Any?) : Action()
        class Error(val error: ErrorModel?) : Action()
    }

    override fun onReduceState(viewAction: Action) = when (viewAction) {
        is Action.Error -> state.copy(
            isLoading = false,
            isError = true,
            error = viewAction.error,
            loginData = null
        )

        is Action.LoginApiData -> state.copy(
            isLoading = false,
            isError = false,
            error = null,
            loginData = viewAction.data
        )
    }
}