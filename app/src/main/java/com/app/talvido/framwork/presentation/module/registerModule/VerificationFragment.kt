package com.app.talvido.framwork.presentation.module.registerModule

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.app.talvido.R
import com.app.talvido.base.presentation.extensions.observe
import com.app.talvido.base.presentation.fragment.BaseContainerFragment
import com.app.talvido.business.localData.PrefsManager
import com.app.talvido.databinding.FragmentVerificationBinding
import com.app.talvido.framwork.datasource.model.AuthResponse
import com.app.talvido.framwork.datasource.requestModel.AuthRequestModel
import com.app.talvido.framwork.presentation.module.loginModule.LoginViewModel
import com.app.talvido.framwork.presentation.navHost.HomeActivity
import com.app.talvido.utils.toast
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi


/**
 * A simple [Fragment] subclass.
 * Use the [VerificationFragment.newInstance] factory method to
 * create an instance of this fragment.
 */

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class VerificationFragment : BaseContainerFragment<FragmentVerificationBinding>() {

    private lateinit var binding: FragmentVerificationBinding

    override val layoutResourceId: Int
        get() = R.layout.fragment_verification


    private val viewModel:LoginViewModel by viewModels()

    private lateinit var authRequestModel: AuthRequestModel
    private var otp = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = getViewDataBinding()

        otp = arguments?.getString("otp").toString()
        authRequestModel = arguments?.getSerializable("details") as AuthRequestModel


        binding.etOtp1.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (binding.etOtp1.text.toString().length == 1) {
                    binding.etOtp1.clearFocus()
                    binding.etOtp2.requestFocus()
                    binding.etOtp2.setCursorVisible(true)
                }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                if (binding.etOtp1.text.toString().length == 0) {
                    binding.etOtp1.requestFocus()
                }
            }
        })


        binding.etOtp2.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (binding.etOtp2.text.toString().length == 1) {
                    binding.etOtp2.clearFocus()
                    binding.etOtp3.requestFocus()
                    binding.etOtp3.setCursorVisible(true)

                }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun afterTextChanged(s: Editable) {
                if (binding.etOtp2.text.toString().length == 0) {
                    binding.etOtp1.requestFocus()
                }

            }
        })

        binding.etOtp3.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (binding.etOtp3.text.toString().length == 1) {
                    binding.etOtp3.clearFocus()
                    binding.etOtp4.requestFocus()
                    binding.etOtp4.setCursorVisible(true)
                }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun afterTextChanged(s: Editable) {
                if (binding.etOtp3.text.toString().length == 0) {
                    binding.etOtp2.requestFocus()
                }

            }
        })


        binding.etOtp4.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (binding.etOtp4.text.toString().length == 0) {
                    binding.etOtp3.requestFocus()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (binding.etOtp4.text.toString().length == 1) {
                    binding.etOtp4.requestFocus()
                    binding.etOtp4.setCursorVisible(true)

                    HitApi()
                }
            }
        })

        observe(viewModel.stateLiveData,observer)
    }

    private fun HitApi() {
        var observeOtp =
            binding.etOtp1.text.toString() + binding.etOtp2.text.toString() + binding.etOtp3.text.toString() + binding.etOtp4.text.toString()

        if (observeOtp==otp){
            showLoading()
            viewModel.registerUser(authRequestModel)
        }else{
            "Please enter valid OTP".toast()
        }
    }

    private val observer=Observer<LoginViewModel.ViewState>{
        hideLoading()
        if (it.isError){
            it.error?.message?.toast()
        }else{
            it.loginData?.let {response->
                var result=response as AuthResponse
                PrefsManager.get().save(PrefsManager.PREF_USER_PROFILE,Gson().toJson(result))
                PrefsManager.get().save(PrefsManager.SESSION,true)
                PrefsManager.get().save(PrefsManager.PREF_AUTH_TOKEN,result.token)
                startActivity(Intent(requireActivity(),HomeActivity::class.java))
                requireActivity().finish()
            }
        }
    }
}