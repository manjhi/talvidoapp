package com.app.talvido.framwork.presentation.module.loginModule

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.app.talvido.R
import com.app.talvido.base.presentation.extensions.observe
import com.app.talvido.base.presentation.fragment.BaseContainerFragment
import com.app.talvido.business.localData.PrefsManager
import com.app.talvido.databinding.FragmentLoginBinding
import com.app.talvido.framwork.datasource.model.AuthResponse
import com.app.talvido.framwork.datasource.requestModel.AuthRequestModel
import com.app.talvido.framwork.presentation.navHost.HomeActivity
import com.app.talvido.utils.deviceId
import com.app.talvido.utils.onClick
import com.app.talvido.utils.toast
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi


@ExperimentalCoroutinesApi
@AndroidEntryPoint
class LoginFragment : BaseContainerFragment<FragmentLoginBinding>() {


    private lateinit var binding: FragmentLoginBinding

    override val layoutResourceId: Int
        get() = R.layout.fragment_login

    private val viewModel: LoginViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = getViewDataBinding()
        binding.vm = viewModel

        binding.signUpButton.onClick {
            moveNext(R.id.action_loginFragment_to_registerFragment)
        }

        binding.forgetPassword.onClick {
            moveNext(R.id.action_loginFragment_to_forgotPasswordFragment)
        }

        binding.loginButton.onClick {
//            startActivity(Intent(requireActivity(),HomeActivity::class.java))
            if (ValidateCheck()) {
                HitApi()
            }
        }


        observe(viewModel.stateLiveData, observer)
    }

    private fun HitApi() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener {
            it.addOnSuccessListener {
                var authRequestModel = AuthRequestModel(
                    deviceId = deviceId(),
                    phone = "${binding.ccp.selectedCountryCodeWithPlus}${binding.etEmailPhone.text}",
                    password = binding.etPassLogin.text.toString(),
                    reg_id = it
                )
                showLoading()
                viewModel.login(authRequestModel)
            }
        }
    }

    private fun ValidateCheck(): Boolean {
        if (binding.etEmailPhone.text.isNullOrEmpty()) {
            "Please enter phone number".toast()
            return false
        }
        if (binding.etPassLogin.text.isNullOrEmpty()) {
            "Please enter password".toast()
            return false
        }
        return true
    }

    private val observer = Observer<LoginViewModel.ViewState> {
        hideLoading()
        if (it.isError) {
            it.error?.message?.toast()
        } else {
            it.loginData?.let { response ->
                var result = response as AuthResponse
                PrefsManager.get().save(PrefsManager.PREF_USER_PROFILE, Gson().toJson(result))
                PrefsManager.get().save(PrefsManager.SESSION, true)
                PrefsManager.get().save(PrefsManager.USER_AUTH, result.token)
                PrefsManager.get().save(PrefsManager.PREF_AUTH_TOKEN, result.token)

                startActivity(Intent(requireActivity(), HomeActivity::class.java))
                requireActivity().finish()
            }
        }
    }
}