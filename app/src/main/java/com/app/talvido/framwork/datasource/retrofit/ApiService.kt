package com.app.talvido.framwork.datasource.retrofit


import com.app.talvido.framwork.datasource.model.ApiResponse
import com.google.gson.JsonObject
import com.app.talvido.framwork.datasource.model.AuthResponse
import com.app.talvido.framwork.datasource.requestModel.AuthRequestModel
import com.app.talvido.framwork.datasource.requestModel.PublishLiveModel
import com.app.talvido.liveStream.models.PublishLiveResponse
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*


interface ApiService {

    @POST(ApiConstant.VALIDATE_PHONE)
    suspend fun validatePhone(@Body authRequestModel: AuthRequestModel): Response<ApiResponse<Any>>

    @POST(ApiConstant.LOGIN)
    suspend fun loginUser(
        @Body authRequestModel: AuthRequestModel
    ): Response<ApiResponse<AuthResponse>>

    @POST(ApiConstant.REGISTER)
    suspend fun registerUser(@Body authRequestModel: AuthRequestModel): Response<ApiResponse<AuthResponse>>


    //not implemented because of some issue
    @POST(ApiConstant.GO_LIVE)
    suspend fun publishStream(
        @Body publishLiveModel: PublishLiveModel
    ): Response<ApiResponse<PublishLiveResponse>>

   @GET(ApiConstant.STOP_LIVE)
    suspend fun stopLiveStream(@Query("liveId") liveId: String): Response<ApiResponse<Any>>


    //without viewmodel- Own classes made by me
    @POST(ApiConstant.GO_LIVE)
     fun publishStreaming(
        @Body publishLiveModel: PublishLiveModel
    ): Call<JsonObject>

    @GET(ApiConstant.STOP_LIVE)
     fun stopLiveStreaming(@Query("liveId") liveId: String): Call<JsonObject>
}