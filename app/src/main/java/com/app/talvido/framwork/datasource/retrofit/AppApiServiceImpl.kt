package com.app.talvido.framwork.datasource.retrofit


import com.google.gson.JsonObject
import com.app.talvido.framwork.datasource.model.ApiResponse
import com.app.talvido.framwork.datasource.model.AuthResponse
import com.app.talvido.framwork.datasource.requestModel.AuthRequestModel
import com.app.talvido.framwork.datasource.requestModel.PublishLiveModel
import com.app.talvido.liveStream.models.PublishLiveResponse
import retrofit2.Response

/**
 * Created by Manjinder Singh on 05,January,2021
 */

class AppApiServiceImpl
constructor(
    private val apiService: ApiService
) : AppApiServices {
    override suspend fun login(authRequestModel: AuthRequestModel): Response<ApiResponse<AuthResponse>> {
        return apiService.loginUser(authRequestModel)
    }

    override suspend fun validatePhone(authRequestModel: AuthRequestModel): Response<ApiResponse<Any>> {
        return apiService.validatePhone(authRequestModel)
    }

    override suspend fun registerUser(authRequestModel: AuthRequestModel): Response<ApiResponse<AuthResponse>> {
        return apiService.registerUser(authRequestModel)
    }

    override suspend fun publishLive(publishLiveModel: PublishLiveModel): Response<ApiResponse<PublishLiveResponse>> {
        return apiService.publishStream(publishLiveModel)
    }

    override suspend fun stopLiveStream(liveId: String): Response<ApiResponse<Any>> {
        return apiService.stopLiveStream(liveId)
    }


}