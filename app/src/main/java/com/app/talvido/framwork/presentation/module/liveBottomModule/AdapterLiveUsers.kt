package com.app.talvido.framwork.presentation.module.liveBottomModule

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.talvido.R

class AdapterLiveUsers(
    var context: Context,
    var list: ArrayList<String>,
    var liveClicked: LiveCallBack
) : RecyclerView.Adapter<AdapterLiveUsers.MyViewHolder>() {

    interface LiveCallBack {
        fun onLiveUserClicked()
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_live_users, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.setOnClickListener { liveClicked.onLiveUserClicked() }
    }

    override fun getItemCount(): Int = list.size

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getItemViewType(position: Int): Int = position

    fun updateList(list: ArrayList<String>) {
        this.list.addAll(list)
        notifyDataSetChanged()
    }

}