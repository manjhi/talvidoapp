package com.app.talvido.framwork.datasource.retrofit

object ApiConstant {

    const val VALIDATE_PHONE="phoneValidate"
    const val REGISTER="register"
    const val LOGIN="login"
    const val GO_LIVE="goLive"
    const val STOP_LIVE="stopLive"
}