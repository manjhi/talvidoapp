package com.app.talvido.framwork.presentation.module.registerModule

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.app.talvido.R
import com.app.talvido.base.presentation.extensions.observe
import com.app.talvido.base.presentation.fragment.BaseContainerFragment
import com.app.talvido.databinding.FragmentRegisterBinding
import com.app.talvido.framwork.datasource.requestModel.AuthRequestModel
import com.app.talvido.framwork.presentation.module.loginModule.LoginViewModel
import com.app.talvido.utils.deviceId
import com.app.talvido.utils.onClick
import com.app.talvido.utils.toast
import com.google.firebase.messaging.FirebaseMessaging
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import timber.log.Timber

/**
 * A simple [Fragment] subclass.
 * Use the [RegisterFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@ExperimentalCoroutinesApi
@AndroidEntryPoint
class RegisterFragment : BaseContainerFragment<FragmentRegisterBinding>() {


    private lateinit var binding: FragmentRegisterBinding

    override val layoutResourceId: Int
        get() = R.layout.fragment_register

    private val viewModel: LoginViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = getViewDataBinding()

        binding.backButton.onClick {
            moveBack()
        }


        binding.senOtpButton.onClick {
            if (validateData()) {
                showLoading()
                var authRequestModel = AuthRequestModel(
                    phone = "${binding.ccp.selectedCountryCodeWithPlus}${binding.etEmailPhone.text}"
                )
                viewModel.validatePhone(authRequestModel)
            }
        }


        observe(viewModel.stateLiveData, observer)
    }

    private fun validateData(): Boolean {
        if (binding.etEmailPhone.text.toString().isNullOrEmpty()) {
            "Please enter phone number".toast()
            return false
        }
        if (binding.etName.text.isNullOrEmpty()) {
            "Please enter name".toast()
            return false
        }
        if (binding.etPassLogin.text.toString().isNullOrEmpty()) {
            "Please enter password".toast()
            return false
        }

        if (binding.etPassLogin.text.toString().length < 7) {
            "Please enter minimum 8 character password".toast()
            return false
        }

        if (binding.confirmPass.text.toString().isNullOrEmpty()) {
            "Please enter confirm password".toast()
            return false
        }

        if (!binding.confirmPass.text.toString()
                .contentEquals(binding.etPassLogin.text.toString())
        ) {
            "Password and confirm password should be same".toast()
            return false
        }
        return true
    }

    private val observer = Observer<LoginViewModel.ViewState> {
        hideLoading()
        if (it.isError) {
            it.error?.message?.toast()
        } else {
            it.loginData?.let { otp ->
                otp.toString().toast()
                FirebaseMessaging.getInstance().token.addOnCompleteListener {
                    it.addOnSuccessListener {
                        var authRequestModel = AuthRequestModel(
                            phone = "${binding.ccp.selectedCountryCodeWithPlus}${binding.etEmailPhone.text}",
                            deviceId = deviceId(),
                            name = binding.etName.text.toString(),
                            password = binding.etPassLogin.text.toString(),
                            country = "${binding.ccp.selectedCountryName}",
                            reg_id = it
                        )
                        var bundle = Bundle()
                        bundle.putSerializable("details", authRequestModel)
                        bundle.putString("otp", otp.toString())
                        moveNextArgs(R.id.action_registerFragment_to_verificationFragment, bundle)
                    }
                }
            }
        }
    }
}

