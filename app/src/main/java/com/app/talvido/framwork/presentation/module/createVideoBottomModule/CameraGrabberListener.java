package com.app.talvido.framwork.presentation.module.createVideoBottomModule;

public interface CameraGrabberListener {
    void onCameraInitialized();
    void onCameraError(String errorMsg);
}
