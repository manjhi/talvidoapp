package com.app.talvido.framwork.presentation.module.videoBottomModeul

import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.talvido.R
import com.app.talvido.base.presentation.fragment.BaseContainerFragment
import com.app.talvido.databinding.FragmentVideoBottomBinding


/**
 * A simple [Fragment] subclass.
 * Use the [VideoBottomFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class VideoBottomFragment : BaseContainerFragment<FragmentVideoBottomBinding>() {

    private lateinit var binding: FragmentVideoBottomBinding

    override val layoutResourceId: Int
        get() = R.layout.fragment_video_bottom

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = getViewDataBinding()

        binding.videoShimmer.videoEffect.startShimmer()

        Handler().postDelayed({
            binding.videoShimmer.videoEffect.stopShimmer()
        },10000)

    }
}