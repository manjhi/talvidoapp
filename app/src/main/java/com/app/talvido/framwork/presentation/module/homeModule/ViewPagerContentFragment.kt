package com.app.talvido.framwork.presentation.module.homeModule

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.talvido.R
import com.app.talvido.utils.INTRO_STRING_OBJECT
import kotlinx.android.synthetic.main.fragment_view_pager_content.*

/**
 * A simple [Fragment] subclass.
 * Use the [ViewPagerContentFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ViewPagerContentFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_view_pager_content, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        arguments?.takeIf { it.containsKey("INTRO_STRING_OBJECT") }?.apply {
            view_pager_header.text = getStringArray(INTRO_STRING_OBJECT)!![0]
            view_pager_sub_text.text = getStringArray(INTRO_STRING_OBJECT)!![1]
            changeColor(getStringArray("INTRO_STRING_OBJECT")!![2])
        }
    }

    fun changeColor(color:String){
        when(color)
        {
            "R" ->
                base_layout.setBackgroundColor(resources.getColor(android.R.color.holo_red_light))
            "B" ->
                base_layout.setBackgroundColor(resources.getColor(android.R.color.holo_blue_dark))
            "G" ->
                base_layout.setBackgroundColor(resources.getColor(android.R.color.holo_green_dark))
        }
    }
}