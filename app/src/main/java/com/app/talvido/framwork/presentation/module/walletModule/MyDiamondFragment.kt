package com.app.talvido.framwork.presentation.module.walletModule

import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.talvido.R
import com.app.talvido.base.presentation.fragment.BaseContainerFragment
import com.app.talvido.databinding.FragmentMyDiamondBinding


/**
 * A simple [Fragment] subclass.
 * Use the [MyDiamondFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MyDiamondFragment : BaseContainerFragment<FragmentMyDiamondBinding>() {

    private lateinit var binding: FragmentMyDiamondBinding

    override val layoutResourceId: Int
        get() = R.layout.fragment_my_diamond


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = getViewDataBinding()

        binding.swipeView.setOnRefreshListener {
            Handler().postDelayed({
                binding.swipeView.isRefreshing = false
            }, 2000)
        }
    }
}