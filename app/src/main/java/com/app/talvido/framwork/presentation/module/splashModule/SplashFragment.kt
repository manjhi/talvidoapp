package com.app.talvido.framwork.presentation.module.splashModule

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.AnimationSet
import com.app.talvido.R
import com.app.talvido.base.presentation.fragment.BaseContainerFragment
import com.app.talvido.business.localData.PrefsManager
import com.app.talvido.databinding.FragmentSplashBinding
import com.app.talvido.framwork.datasource.retrofit.ApiConstant
import com.app.talvido.framwork.presentation.navHost.HomeActivity
import com.app.talvido.utils.Visible
import com.app.talvido.utils.deviceId
import com.google.firebase.messaging.FirebaseMessaging
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import timber.log.Timber

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class SplashFragment : BaseContainerFragment<FragmentSplashBinding>() {

    private lateinit var binding: FragmentSplashBinding

    override val layoutResourceId: Int
        get() = R.layout.fragment_splash


    private lateinit var handler: Handler
    private lateinit var runnable: Runnable


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = getViewDataBinding()


        Timber.e("CREATE : ${deviceId()}")



        handler = Handler(Looper.getMainLooper())

        var fadeIn = AlphaAnimation(0.0f, 2.0f)
        var fadeOut = AlphaAnimation(1.0f, 0.0f)
        var set = AnimationSet(false)
        set.addAnimation(fadeIn)
//        set.addAnimation(fadeOut)
        fadeOut.startOffset = 2000
        set.duration = 5000
        runnable = Runnable {
            binding.splashLogo.Visible()
            binding.splashLogo.startAnimation(set)
        }

        set.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {

            }

            override fun onAnimationEnd(animation: Animation?) {
                if (PrefsManager.get().getBoolean(PrefsManager.SESSION,false)){
                    startActivity(Intent(requireActivity(), HomeActivity::class.java))
                    requireActivity().finish()
                }else {
                    moveNext(R.id.action_splashFragment_to_loginFragment)
                }
            }

            override fun onAnimationStart(animation: Animation?) {
            }
        })

        handler.postDelayed(runnable, 100)
    }
}