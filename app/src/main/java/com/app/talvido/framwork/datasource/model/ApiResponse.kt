package com.app.talvido.framwork.datasource.model



data class ApiResponse<T>(
    val status: Boolean,
    val message: String?=null,
    val result: T
)