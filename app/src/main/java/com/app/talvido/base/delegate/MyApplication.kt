package com.app.talvido.base.delegate

import android.app.Application
import android.content.Context
import com.app.talvido.business.localData.PrefsManager
import com.app.talvido.liveStream.firebase.PostValueListener
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber

@HiltAndroidApp
class MyApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        application = this
        PrefsManager.initialize(this)
        initTimber()
    }

    private fun initTimber() {
//        if (BuildConfig.DEBUG) {
        Timber.plant(Timber.DebugTree())
//        }
    }

    companion object {
        lateinit var application: Context

        private var postValueListener: PostValueListener? = null

        @JvmName("getPostValueListener1")
        fun getPostValueListener(): PostValueListener? {
            return postValueListener
        }

        @JvmName("setPostValueListener1")
        fun setPostValueListener(postValueListener: PostValueListener) {
            Companion.postValueListener = postValueListener
        }
    }
}