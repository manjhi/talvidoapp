package com.app.talvido.base.presentation.fragment


import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.findNavController
import com.app.talvido.base.presentation.activity.BaseActivity
import com.app.talvido.utils.Constants.Companion.latitude
import com.app.talvido.utils.Constants.Companion.longitude
import com.app.talvido.business.localData.PrefsManager
import com.app.talvido.framwork.datasource.model.AuthResponse
import com.app.talvido.utils.LoadingDialog
import com.app.talvido.utils.LocationOn
import com.google.android.gms.location.*
import timber.log.Timber
import java.lang.Exception

abstract class BaseContainerFragment<T : ViewDataBinding> : Fragment() {


    private var mViewDataBinding: T? = null
    private var mRootView: View? = null
    lateinit var mFusedLocationClient: FusedLocationProviderClient
    var lastLatitude: Double? = 0.0
    var lastLongitude: Double? = 0.0

    @get:LayoutRes
    protected abstract val layoutResourceId: Int

    private val loadingDialog: LoadingDialog by lazy {
        LoadingDialog(requireActivity())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mViewDataBinding = DataBindingUtil.inflate(inflater, layoutResourceId, container, false)
        mRootView = mViewDataBinding?.root.also {
            Timber.v("onCreateView ${javaClass.simpleName}")
        }
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        return mRootView
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mRootView = null
        mViewDataBinding?.lifecycleOwner = null
        mViewDataBinding = null

        System.gc()
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewDataBinding!!.lifecycleOwner = this
        mViewDataBinding!!.executePendingBindings()
    }

    open fun getViewDataBinding(): T {
        return mViewDataBinding!!
    }

    fun moveNext(id: Int) {
        try {
            findNavController().navigate(id)
        } catch (e: Exception) {
            Timber.e(e.message)
        }
    }

    fun moveNextArgs(id: Int, bundle: Bundle) {
        try {
            findNavController().navigate(id, bundle)
        } catch (e: Exception) {
            Timber.e(e.message)
        }
    }

    fun moveBack() {
        requireActivity().onBackPressed()
    }

    fun showLoading() {
        loadingDialog.setLoading(true)
    }

    fun hideLoading() {
        loadingDialog.setLoading(false)
    }

    fun showToast(msg: String) {
        Toast.makeText(requireActivity(), msg, Toast.LENGTH_SHORT).show()
    }

    /*get location starts*/
    private fun checkPermissions(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }

    @SuppressLint("MissingPermission")
    fun getLastLocation() {
        if (checkPermissions()) {
            if (isLocationEnabled()) {

                mFusedLocationClient.lastLocation.addOnCompleteListener(requireActivity()) { task ->

                    var location: Location? = task.result

                    if (location == null) {
                        requestNewLocationData()
                    } else {

                        latitude = location.latitude
                        longitude = location.longitude

                        lastLatitude = location.latitude
                        lastLongitude = location.longitude
                        sendLocationBroadCast()
                        Log.i("lastLatLng", "getLastLocation: $lastLatitude $lastLongitude")
                    }
                }
            } else {
                Log.e("Javaaaaaa", javaClass.simpleName.toString())
                if (javaClass.simpleName.equals("LandingActivty")) {
                    LocationPop()
                }
            }
        }/* else {
            requestPermissions()
        }*/
    }

    @SuppressLint("MissingPermission")
    private fun requestNewLocationData() {
        var mLocationRequest = LocationRequest()
        mLocationRequest.interval = 500
        mLocationRequest.fastestInterval = 500
        mLocationRequest.numUpdates = 3

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        mFusedLocationClient!!.requestLocationUpdates(
            mLocationRequest, mLocationCallback,
            Looper.myLooper()
        )
    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            var mLastLocation: Location = locationResult.lastLocation

            latitude = mLastLocation.latitude
            longitude = mLastLocation.longitude

            lastLatitude = mLastLocation.latitude
            lastLongitude = mLastLocation.longitude
            sendLocationBroadCast()
            Log.i("lastLatLng", "onLocationResult: $lastLatitude $lastLongitude")
        }
    }

    private fun isLocationEnabled(): Boolean {
        var locationManager: LocationManager =
            requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    private fun sendLocationBroadCast() {
        val intent = Intent("location_Broadcast")
        LocalBroadcastManager.getInstance(requireActivity()).sendBroadcast(intent)
    }

    var dialog: AlertDialog.Builder? = null
    var builder: AlertDialog? = null
    private fun LocationPop() {
        if (!LocationOn.isLocationEnabled(requireActivity())) {
            // notify user
            dialog = AlertDialog.Builder(requireActivity())
            dialog?.setMessage("Location not enabled!")
            dialog?.setPositiveButton("Open location settings")
            { paramDialogInterface, paramInt ->
                val myIntent =
                    Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(myIntent)
                paramDialogInterface.dismiss()
            }
            dialog?.setNegativeButton("Cancel")
            { paramDialogInterface, paramInt ->
                // TODO Auto-generated method stub
                paramDialogInterface.dismiss()
            }
            dialog?.show()
        }
    }
    /*Get location ends*/
}